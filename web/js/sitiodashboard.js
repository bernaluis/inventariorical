$(".button-collapse").sideNav();
$('#fechaFin').pickadate({
	format: 'yyyy-mm-dd',
	formatSubmit: 'yyyy/mm/dd'
});
$('#fechaIni').pickadate({
	format: 'yyyy-mm-dd',
	formatSubmit: 'yyyy-mm-dd'
});
$('#horaDesde').pickatime({
  twelvehour: false,
  darktheme: true
});
$('#horaHasta').pickatime({
  twelvehour: false,
  darktheme: true
});
   
$(document).ready(function() {
  $('.mdb-select').material_select();
  $('.js-example-basic-single').select2();
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
