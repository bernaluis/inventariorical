$('.dropdown-toggle').dropdown();

$(".button-collapse").sideNav();

// Data Picker Initialization
$('.datepicker').pickadate({
    monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    weekdaysShort: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
    weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    labelMonthNext: 'Siguiente mes',
    labelMonthPrev: 'Mes anterior',
    selectYears: true,
    selectMonths: true,
    format: 'yyyy-mm-dd',
    formatSubmit: 'yyyy-mm-dd',
    today: 'Hoy',
    clear: 'Limpiar',
    close: 'Cancelar',
    selectYears: 60,
    min: [1940,1,1],
    max: true
});