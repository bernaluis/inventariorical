create database inventariorical;
create table tipo_usuario(
    id_tipou int not null primary key AUTO_INCREMENT,
    tipo_usua varchar(50) not null,
    estado int not null
);
create table estado_movimiento(
    id_estadom int not null PRIMARY key AUTO_INCREMENT,
    estado varchar(50)
);
create table tipo_producto(
    id_tipop int not null primary key AUTO_INCREMENT,
    tipo_producto varchar(50) not null,
    estado int not null
);
create table marca(
    id_marca int not null primary key AUTO_INCREMENT,
    marca varchar(50) not null,
    estado int not null
);
create table bodega(
    id_bodega int not null primary key AUTO_INCREMENT,
    bodega varchar(50) not null,
    estado int not null
);
create table estado_producto(
    id_estadop int not null primary key AUTO_INCREMENT,
    estado_producto varchar(50) not null,
    estado int not null
);
create table modelo(
    id_modelo int not null primary key AUTO_INCREMENT,
    modelo varchar(100) not null,
    id_marca int not null,
    estado int not null,
    FOREIGN key(id_marca) REFERENCES marca(id_marca)
);
create table procedencia(
    id_procedencia int not null primary key AUTO_INCREMENT,
    procedencia varchar(50) not null,
    estado int not null
);
create table usuarios(
    id_usuario int not null primary key AUTO_INCREMENT,
    codigo varchar(8) not null UNIQUE,
    correo varchar(50) not null UNIQUE,
    nombres varchar(50) not null,
    apellidos varchar(50) not null,
    pass varchar(200) not null,
    id_tipou int not null,
    estado int not null,
    FOREIGN key(id_tipou) REFERENCES tipo_usuario(id_tipou)
);
create table producto(
    id_producto int not null primary key AUTO_INCREMENT,
    codigo varchar(50),
    producto varchar(50) not null,
    descripcion varchar(100) not null,
    id_modelo int not null,
    id_tipop int not null,
    estado int not null,
    FOREIGN key(id_modelo) REFERENCES modelo(id_modelo),
    FOREIGN key(id_tipop) REFERENCES tipo_producto(id_tipop)
);
create table inventario(
    id_inventario int not null PRIMARY KEY AUTO_INCREMENT,
    id_producto int not null,
    id_estadop int not null,
    id_bodega int not null,
    cantidad INT,
    FOREIGN key(id_estadop) REFERENCES estado_producto(id_estadop),
    FOREIGN key(id_bodega) REFERENCES bodega(id_bodega),
    FOREIGN key(id_producto)  REFERENCES producto(id_producto)
);
create table tipo_movimiento(
    id_tipom int not null primary key AUTO_INCREMENT,
    tipo_movimiento varchar(20),
    estado int not null
);
/*estado 4 borrado, estado 1 no procesado, estado 2 aceptado, estado 3 denegado */
create table movimiento(
    id_movimiento int not null primary key AUTO_INCREMENT,
    id_producto INT NOT NULL,
    id_usuario int not null,
    cantidad int not null,
    id_procedencia int,
    fecha date not null,
    descripcion varchar(200),
    usuarioa varchar(8) null,
    id_estadom int not null,
    id_tipom INT NOT NULL,
    id_bodega int not null,
    id_estadop int not null,
    FOREIGN key(id_producto) REFERENCES producto(id_producto),
    FOREIGN key(id_usuario) REFERENCES usuarios(id_usuario),
    FOREIGN key(id_procedencia) REFERENCES procedencia(id_procedencia),
    FOREIGN KEY(id_tipom) references tipo_movimiento(id_tipom),
    FOREIGN KEY(id_bodega) references bodega(id_bodega),
    FOREIGN KEY(id_estadop) references estado_producto(id_estadop),
    FOREIGN key(id_estadom) REFERENCES estado_movimiento(id_estadom)
);

INSERT INTO `tipo_movimiento` (`id_tipom`, `tipo_movimiento`, `estado`) VALUES (NULL, 'Ingreso', '1');
INSERT INTO `tipo_movimiento` (`id_tipom`, `tipo_movimiento`, `estado`) VALUES (NULL, 'Retiro', '1');
INSERT INTO `tipo_movimiento` (`id_tipom`, `tipo_movimiento`, `estado`) VALUES (NULL, 'Prestamo', '1');
INSERT INTO `tipo_usuario`( `tipo_usua`,`estado`) VALUES ('Administrador', '1');
INSERT INTO `tipo_usuario`( `tipo_usua`,`estado`) VALUES ( 'Empleado', '1');
INSERT INTO `usuarios` ( `codigo`, `correo`, `nombres`, `apellidos`, `pass`, `id_tipou`, `estado`) VALUES ('AD1000', 'admin@gmail.com', 'Admin', 'Admin', '$2y$10$1N/fV/S86rHBuPZKXZpmJu6RqiUSZ9WdkPRHc/Kz7HdnQy86ncIMe', '1', '1');
INSERT INTO `estado_movimiento` (`id_estadom`, `estado`) VALUES (NULL, 'No procesado');
INSERT INTO `estado_movimiento` (`id_estadom`, `estado`) VALUES (NULL, 'Aceptado');
INSERT INTO `estado_movimiento` (`id_estadom`, `estado`) VALUES (NULL, 'Rechazado');
INSERT INTO `estado_movimiento` (`id_estadom`, `estado`) VALUES (NULL, 'Borrado');