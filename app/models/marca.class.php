<?php
class Marca extends Validator{
    private $id=null;
    private $marca=null;

    public function setId($value){
		if($this->validateId($value)){
			$this->id = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getId(){
		return $this->id;
    }
    public function setMarca($value){
		if($this->validateAlphabetic($value,1,50)){
			$this->marca = $value;
			return true;
		}else{
			return false;
			$this->marca = $value;
		}
	}
	public function getMarca(){
		return $this->marca;
    }

  public function consultarMarcaLimite($empezarDesde,$tamanioPag)
	{
		$sql = "SELECT id_marca,marca FROM `marca` where estado=1   LIMIT $empezarDesde,$tamanioPag";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function consultarMarca()
	{
		$sql = "SELECT id_marca,marca FROM `marca` where estado=1";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function verificarDelete()
	{
		$sql = "SELECT id_modelo,modelo FROM `modelo` where estado=1 and id_marca=?";
		$params = array($this->id);
		return Database::getRows($sql, $params);
	}

	public function searchMarca($value)
	{
		$sql = "SELECT id_marca,marca FROM `marca` where estado=1 and marca like ? ";
		$params = array("%$value%");
		return Database::getRows($sql, $params);
	}
	public function insertMarca()
	{
		$sql = "INSERT INTO `marca` (`id_marca`, `marca`,estado) VALUES (NULL, ?,1)";
		$params = array($this->marca);	
		return Database::executeRow($sql, $params);
	}
	public function borrarMarca()
	{
		$sql = "update marca set estado=2 where id_marca=?";
		$params = array($this->id);	
		return Database::executeRow($sql, $params);
	}
	public function updateMarca()
	{
		$sql = "UPDATE `marca` SET `marca` =? WHERE id_marca = ?";
		$params = array($this->marca,$this->id);	
		return Database::executeRow($sql, $params);
	}
	public function readMarca()
	{
			$sql    = "SELECT id_marca,marca FROM `marca` where estado=1 and id_marca=? ";
			$params = array($this->id);
			$user   = Database::getRow($sql, $params);
			if ($user) {
					$this->marca   = $user['marca'];
					return true;
			} else {
					return null;
			}
	}


}
?>