<?php
class Inventario extends Validator{
	//Declaración de propiedades
	private $id = null;
	private $cantidad = null;
	private $id_producto = null;
	private $bodega = null;
	private $estado = null;
    //Métodos para sobrecarga de propiedades

	public function setId($value){
		if($this->validateId($value)){
			$this->id = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getId(){
		return $this->id;
	}
	public function setIdEstado($value){
		if($this->validateId($value)){
			$this->estado = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdEstado(){
		return $this->estado;
	}
	
	public function setIdBodega($value){
		if($this->validateId($value)){
			$this->bodega = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdBodega(){
		return $this->bodega;
	}


	public function setCantidad($value){
		if($this->validateAlphanumeric($value, 1, 50)){
			$this->cantidad = $value;
			return true;
		}else{
			return false;
			$this->cantidad= $value;
		}
	}
	public function getCantidad(){
		return $this->cantidad;
    }



	public function setIdProducto($value){
		if($this->validateId($value)){
			$this->id_producto = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdProducto(){
		return $this->id_producto;
	}
	//Metodos para el manejo del SCRUD

	public function consultarInventarioLimite($empezarDesde,$tamanioPag)
	{
		$sql = "select producto.codigo, inventario.id_inventario,estado_producto.estado_producto,bodega.bodega,inventario.cantidad,producto.id_producto,producto.producto,modelo.modelo,marca.marca from inventario inner join producto inner join modelo inner join marca inner join estado_producto inner join bodega where marca.id_marca=modelo.id_marca and producto.id_producto=inventario.id_producto and producto.id_modelo=modelo.id_modelo and bodega.id_bodega=inventario.id_bodega and estado_producto.id_estadop=inventario.id_estadop and producto.estado=1 LIMIT $empezarDesde,$tamanioPag";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function readInventario(){
		$sql = "select producto.codigo, inventario.id_inventario,estado_producto.estado_producto,bodega.bodega,inventario.cantidad,producto.id_producto,producto.producto,modelo.modelo,marca.marca from inventario inner join producto inner join modelo inner join marca inner join estado_producto inner join bodega where marca.id_marca=modelo.id_marca and producto.id_producto=inventario.id_producto and producto.id_modelo=modelo.id_modelo and bodega.id_bodega=inventario.id_bodega and estado_producto.id_estadop=inventario.id_estadop and producto.estado=1 and inventario.id_inventario=?";
		$params = array($this->id);
		$user = Database::getRow($sql, $params);
		if($user){
			$this->cantidad= $user['cantidad'];
			$this->id_producto = $user['id_producto'];
			return true;
		}else{
			return null;
		}
	}
	public function searchInventario($value)
	{
		$sql = "select producto.codigo, inventario.id_inventario,estado_producto.estado_producto,bodega.bodega,inventario.cantidad,producto.id_producto,producto.producto,modelo.modelo,marca.marca from inventario inner join producto inner join modelo inner join marca inner join estado_producto inner join bodega where marca.id_marca=modelo.id_marca and producto.id_producto=inventario.id_producto and producto.id_modelo=modelo.id_modelo and bodega.id_bodega=inventario.id_bodega and estado_producto.id_estadop=inventario.id_estadop and producto.estado=1 and  producto.codigo like ?";
		$params = array("%$value%");
		return Database::getRows($sql, $params);
	}
	public function getReport1($bodega,$estado)
	{
		$sql = "select producto.codigo, estado_producto.estado_producto,bodega.bodega,inventario.cantidad,producto.producto,modelo.modelo,marca.marca from inventario inner join producto inner join modelo inner join marca inner join estado_producto inner join bodega where marca.id_marca=modelo.id_marca and producto.id_producto=inventario.id_producto and producto.id_modelo=modelo.id_modelo and bodega.id_bodega=inventario.id_bodega and estado_producto.id_estadop=inventario.id_estadop and producto.estado=1 and inventario.id_bodega=? and inventario.id_estadop=?";
		$params = array($bodega,$estado);
		return Database::getRows($sql, $params);
	}
	public function getReport2($estado)
	{
		$sql = "select producto.codigo,estado_producto.estado_producto,bodega.bodega,inventario.cantidad,producto.producto,modelo.modelo,marca.marca from inventario inner join producto inner join modelo inner join marca inner join estado_producto inner join bodega where marca.id_marca=modelo.id_marca and producto.id_producto=inventario.id_producto and producto.id_modelo=modelo.id_modelo and bodega.id_bodega=inventario.id_bodega and estado_producto.id_estadop=inventario.id_estadop and producto.estado=1 and  inventario.id_estadop=?";
		$params = array($estado);
		return Database::getRows($sql, $params);
	}
	public function getReport3($bodega)
	{
		$sql = "select producto.codigo, estado_producto.estado_producto,bodega.bodega,inventario.cantidad,producto.producto,modelo.modelo,marca.marca from inventario inner join producto inner join modelo inner join marca inner join estado_producto inner join bodega where marca.id_marca=modelo.id_marca and producto.id_producto=inventario.id_producto and producto.id_modelo=modelo.id_modelo and bodega.id_bodega=inventario.id_bodega and estado_producto.id_estadop=inventario.id_estadop and producto.estado=1 and inventario.id_bodega=? ";
		$params = array($bodega);
		return Database::getRows($sql, $params);
	}
	public function getReport4()
	{
		$sql = "select producto.codigo, estado_producto.estado_producto,bodega.bodega,inventario.cantidad,producto.producto,modelo.modelo,marca.marca from inventario inner join producto inner join modelo inner join marca inner join estado_producto inner join bodega where marca.id_marca=modelo.id_marca and producto.id_producto=inventario.id_producto and producto.id_modelo=modelo.id_modelo and bodega.id_bodega=inventario.id_bodega and estado_producto.id_estadop=inventario.id_estadop and producto.estado=1";
		$params = array(null);
		return Database::getRows($sql, $params);
	}

	public function consultarInventario()
	{
		$sql = "select producto.codigo, inventario.id_inventario,estado_producto.estado_producto,bodega.bodega,inventario.cantidad,producto.id_producto,producto.producto,modelo.modelo,marca.marca from inventario inner join producto inner join modelo inner join marca inner join estado_producto inner join bodega where marca.id_marca=modelo.id_marca and producto.id_producto=inventario.id_producto and producto.id_modelo=modelo.id_modelo and bodega.id_bodega=inventario.id_bodega and estado_producto.id_estadop=inventario.id_estadop and producto.estado=1";
		$params = array(null);
		return Database::getRows($sql, $params);
	}


	public function updateInventario(){
		$sql = "UPDATE `inventario` SET `cantidad`=? WHERE id_inventario=?";
		$params = array($this->cantidad, $this->id);
		return Database::executeRow($sql, $params);
	}
	public function deleteInventario(){
		$sql = "delete  from inventario WHERE id_inventario=?";
		$params = array($this->id);
		return Database::executeRow($sql, $params);
	}
	public function insertInventario(){
		$sql = "INSERT INTO `inventario`(`id_inventario`, `id_producto`, `id_estadop`, `id_bodega`, `cantidad`) VALUES (NULL,?,?,?,?)";
		$params = array($this->id_producto,$this->estado,$this->bodega,$this->cantidad);
		return Database::executeRow($sql, $params);
	}



}
?>