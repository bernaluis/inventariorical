<?php
class Usuario extends Validator{
	//Declaración de propiedades
	private $id = null;
    private $nombres = null;
    private $apellidos = null;
    private $codigo = null;
    private $contra=null;
    private $id_tipou = null;
    private $correo=null;

    //Métodos para sobrecarga de propiedades

	public function setId($value){
		if($this->validateId($value)){
			$this->id = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getId(){
		return $this->id;
	}



	public function setNombres($value){
		if($this->validateAlphabetic($value, 1, 50)){
			$this->nombres = $value;
			return true;
		}else{
			return false;
			$this->producto = $value;
		}
	}
	public function getNombres(){
		return $this->nombres;
    }
    public function setApellidos($value){
		if($this->validateAlphabetic($value, 1, 50)){
			$this->apellidos = $value;
			return true;
		}else{
			return false;
			$this->apellidos = $value;
		}
	}
	public function getApellidos(){
		return $this->apellidos;
    }
	public function setIdTipoU($value){
		if($this->validateId($value)){
			$this->id_tipou = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdTipoU(){
		return $this->id_tipou;
	}


	public function setCorreo($value){
		if($this->validateEmail($value)){
			$this->correo = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getCorreo(){
		return $this->correo;
    }
    public function setContra($value){
		if($this->validatePassword($value)){
			$this->contra = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getContra(){
		return $this->contra;
	}
	public function setCodigo($value){
		if($this->validateAlphanumeric($value,1, 50)){
			$this->codigo = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getCodigo(){
		return $this->codigo;
	}

	//Métodos para manejar la sesión del usuario
	
	public function checkUser()
	{
		$sql = "SELECT usuarios.id_usuario,usuarios.id_tipou,usuarios.codigo,usuarios.correo FROM usuarios  where usuarios.codigo=?  and estado=1";
		$params = array($this->codigo);
		$data = Database::getRow($sql, $params);
		if($data){
			$this->id = $data['id_usuario'];
			$this->id_tipou=$data['id_tipou'];
			$this->correo=$data['correo'];
			return true;
		}else{
			return false;
		}
	}
	public function changePasswordRecuperar(){
		$hash = password_hash($this->contra, PASSWORD_DEFAULT);
		$sql = "UPDATE usuarios SET pass = ? WHERE id_usuario = ?";
		$params = array($hash, $this->id);
		return Database::executeRow($sql, $params);
	}
	
	
	public function checkPassword(){
		$sql = "SELECT pass FROM usuarios WHERE codigo = ? AND estado=1";
		$params = array($this->codigo);
		$data = Database::getRow($sql, $params);
		if( password_verify($this->contra, $data['pass'])){
			return true;
		}else{
			return false;
		}
	}
	public function checkPasswordp(){
		$sql = "SELECT pass FROM usuarios WHERE id_usuario = ? AND estado=1";
		$params = array($this->id);
		$data = Database::getRow($sql, $params);
		if( password_verify($this->contra, $data['pass'])){
			return true;
		}else{
			return false;
		}
	}

	public function changePassword(){
		$hash = password_hash($this->contra, PASSWORD_DEFAULT);
		$sql = "UPDATE usuarios SET pass = ? WHERE id_usuario = ?";
		$params = array($hash, $this->id);
		return Database::executeRow($sql, $params);
	}

	public function logOut(){
		return session_destroy();
	}
	public function validateForm($fields){
		foreach($fields as $index => $value){
			$value = trim($value);
			$fields[$index] = $value;
		}
		return $fields;
	}
	//Metodos para manejar el CRUD
	public function getUsuarios(){
		$sql = "select usuarios.id_usuario,usuarios.codigo,usuarios.correo,usuarios.nombres,usuarios.apellidos,tipo_usuario.tipo_usua from usuarios inner join tipo_usuario where tipo_usuario.id_tipou=usuarios.id_tipou and usuarios.estado=1 order by usuarios.codigo";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function getUsuariosVal(){
		$sql = "select usuarios.id_usuario,usuarios.codigo,usuarios.correo,usuarios.nombres,usuarios.apellidos,tipo_usuario.tipo_usua from usuarios inner join tipo_usuario where tipo_usuario.id_tipou=usuarios.id_tipou and usuarios.estado=1 order by usuarios.codigo";
		$params = array(null);
		$data= Database::getRows($sql, $params);
		if( $data){
			return true;
		}else{
			return false;
		}
	}

	public function getUsuariosLimite($empezarDesde,$tamanioPag){
		$sql = "select usuarios.id_usuario,usuarios.codigo,usuarios.correo,usuarios.nombres,usuarios.apellidos,tipo_usuario.tipo_usua from usuarios inner join tipo_usuario where tipo_usuario.id_tipou=usuarios.id_tipou and usuarios.estado=1 order by usuarios.codigo LIMIT $empezarDesde,$tamanioPag";
		$params = array(null);
		return Database::getRows($sql, $params);
	}

	public function insertUsuario(){
		$hash = password_hash($this->getContra(), PASSWORD_DEFAULT);
		$sql = "INSERT INTO `usuarios` (`id_usuario`, `codigo`, `correo`, `nombres`, `apellidos`, `pass`, `id_tipou`, `estado`) VALUES (NULL, ?, ?, ?, ?, ?, ?, 1)";
		$params = array($this->getCodigo(),$this->getCorreo(),$this->getNombres(), $this->getApellidos(),$hash,$this->getIdTipoU());
		return Database::executeRow($sql, $params);
	}
	
	public function searchUsuario($value){
		$sql = "select usuarios.id_usuario,usuarios.codigo,usuarios.correo,usuarios.nombres,usuarios.apellidos,tipo_usuario.tipo_usua from usuarios inner join tipo_usuario where tipo_usuario.id_tipou=usuarios.id_tipou and usuarios.estado=1 and usuarios.nombres like ? or usuarios.apellidos like ? order by usuarios.codigo ";
		$params = array("%$value%", "%$value%");
		return Database::getRows($sql, $params);
	}
	
	public function readUsuario(){
		$sql = "SELECT * FROM usuarios WHERE id_usuario = ?";
		$params = array($this->id);
		$user = Database::getRow($sql, $params);
		if($user){
			$this->codigo = $user['codigo'];
			$this->nombres = $user['nombres'];
			$this->apellidos = $user['apellidos'];
			$this->correo = $user['correo'];
			$this->id_tipou=$user['id_tipou'];
			return true;
		}else{
			return null;
		}
	}
	public function updateUsuario(){
        $hash = password_hash($this->getContra(), PASSWORD_DEFAULT);
		$sql = "UPDATE `usuarios` SET `correo`=?,`nombres`=?,`apellidos`=?,`pass`=?,`id_tipou`=?,codigo=? WHERE id_usuario=?";
		$params = array($this->getCorreo(),$this->getNombres(), $this->getApellidos(),$hash,$this->getIdTipoU(),$this->getCodigo(),$this->id);
		return Database::executeRow($sql, $params);
	}
	public function updateUsuario2(){
        
		$sql = "UPDATE `usuarios` SET `correo`=?,`nombres`=?,`apellidos`=?,`id_tipou`=?,codigo=? WHERE id_usuario=?";
		$params = array($this->getCorreo(),$this->getNombres(), $this->getApellidos(),$this->getIdTipoU(),$this->getCodigo(),$this->id);
		return Database::executeRow($sql, $params);
	}
	public function updatePerfil(){
		$sql = "UPDATE `usuarios` SET `codigo`=?,`correo`=?,`nombres`=?,`apellidos`=? WHERE id_usuario=?";
		$params = array($this->getCodigo(),$this->getCorreo(),$this->getNombres(), $this->getApellidos(),$this->id);
		return Database::executeRow($sql, $params);
	}
	
	public function deleteUsuario()
	{
		$sql = "UPDATE usuarios SET estado=2 WHERE id_usuario = ?";
		$params = array( $this->id);
		return Database::executeRow($sql, $params);
	}	
}
?>