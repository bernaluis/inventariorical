<?php
class Modelo extends Validator{
	//Declaración de propiedades
	private $id = null;
	private $modelo = null;
	private $id_marca = null;

    //Métodos para sobrecarga de propiedades

	public function setId($value){
		if($this->validateId($value)){
			$this->id = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getId(){
		return $this->id;
	}



	public function setModelo($value){
		if($this->validateAlphanumeric($value, 1, 50)){
			$this->modelo = $value;
			return true;
		}else{
			return false;
			$this->modelo = $value;
		}
	}
	public function getModelo(){
		return $this->modelo;
    }



	public function setIdMarca($value){
		if($this->validateId($value)){
			$this->id_marca = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdMarca(){
		return $this->id_marca;
	}
	//Metodos para el manejo del SCRUD

	public function consultarModeloLimite($empezarDesde,$tamanioPag)
	{
		$sql = "SELECT modelo.id_modelo,modelo.modelo,marca.id_marca,marca.marca from modelo INNER JOIN marca where modelo.estado=1 and modelo.id_marca=marca.id_marca LIMIT $empezarDesde,$tamanioPag";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function readModelo(){
		$sql = "SELECT modelo.id_modelo,modelo.modelo,marca.id_marca,marca.marca from modelo INNER JOIN marca where modelo.estado=1 and modelo.id_marca=marca.id_marca and modelo.id_modelo=?";
		$params = array($this->id);
		$user = Database::getRow($sql, $params);
		if($user){
			$this->modelo= $user['modelo'];
			$this->id_marca = $user['id_marca'];
			return true;
		}else{
			return null;
		}
	}
	public function searchModelo($value)
	{
		$sql = "SELECT modelo.id_modelo,modelo.modelo,marca.id_marca,marca.marca from modelo INNER JOIN marca where modelo.estado=1 and modelo.id_marca=marca.id_marca and modelo.modelo like ? ";
		$params = array("%$value%");
		return Database::getRows($sql, $params);
	}
	
	public function consultarModelo()
	{
		$sql = "SELECT modelo.id_modelo,modelo.modelo,marca.id_marca,marca.marca from modelo INNER JOIN marca where modelo.estado=1 and modelo.id_marca=marca.id_marca";
		$params = array(null);
		return Database::getRows($sql, $params);
	}

	public function insertModelo()
	{
		$sql = "INSERT INTO `modelo` (`id_modelo`, `modelo`, `id_marca`, `estado`) VALUES (NULL, ?, ?, 1)";
		$params = array($this->modelo,$this->id_marca);	
		return Database::executeRow($sql, $params);
	}
	public function updateModelo(){
		$sql = "UPDATE modelo SET modelo = ?, id_marca=? WHERE id_modelo = ?";
		$params = array($this->modelo,$this->id_marca, $this->id);
		return Database::executeRow($sql, $params);
	}

	public function deleteModelo(){
		$sql = "UPDATE modelo SET estado = 2 WHERE id_modelo = ?";
		$params = array( $this->id);
		return Database::executeRow($sql, $params);
	}
}
?>