<?php
class TipoProducto extends Validator{
    private $id=null;
    private $tipop=null;

    public function setId($value){
		if($this->validateId($value)){
			$this->id = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getId(){
		return $this->id;
    }
    public function setTipoP($value){
		if($this->validateAlphabetic($value,1,50)){
			$this->tipop = $value;
			return true;
		}else{
			return false;
			$this->tipop = $value;
		}
	}
	public function getTipoP(){
		return $this->tipop;
    }

  public function consultarTipoPLimite($empezarDesde,$tamanioPag)
	{
		$sql = "SELECT id_tipop,tipo_producto FROM `tipo_producto` where estado=1   LIMIT $empezarDesde,$tamanioPag";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function consultarTipoP()
	{
		$sql = "SELECT id_tipop,tipo_producto FROM `tipo_producto` where estado=1";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	

	public function searchTipoP($value)
	{
		$sql = "SELECT id_tipop,tipo_producto FROM `tipo_producto` where estado=1 and tipo_producto like ? ";
		$params = array("%$value%");
		return Database::getRows($sql, $params);
	}
	public function insertTipoP()
	{
		$sql = "INSERT INTO `tipo_producto` (`id_tipop`, `tipo_producto`,estado) VALUES (NULL, ?,1)";
		$params = array($this->tipop);	
		return Database::executeRow($sql, $params);
	}
	public function borrarTipoP()
	{
		$sql = "update tipo_producto set estado=2 where id_tipop=?";
		$params = array($this->id);	
		return Database::executeRow($sql, $params);
	}
	public function updateTipoP()
	{
		$sql = "UPDATE `tipo_producto` SET `tipo_producto` =? WHERE id_tipop = ?";
		$params = array($this->tipop,$this->id);	
		return Database::executeRow($sql, $params);
	}
	public function readTipoP()
	{
			$sql    = "SELECT id_tipop,tipo_producto FROM `tipo_producto` where estado=1 and id_tipop=? ";
			$params = array($this->id);
			$user   = Database::getRow($sql, $params);
			if ($user) {
					$this->tipop   = $user['tipo_producto'];
					return true;
			} else {
					return null;
			}
	}
	public function verificarDelete()
	{
		$sql = "SELECT id_producto,producto FROM `producto` where estado=1 and id_tipop=?";
		$params = array($this->id);
		return Database::getRows($sql, $params);
	}

}
?>