<?php
class TipoMovimiento extends Validator{
    private $id=null;
    private $tipom=null;

    public function setId($value){
		if($this->validateId($value)){
			$this->id = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getId(){
		return $this->id;
    }
    public function setTipoM($value){
		if($this->validateAlphabetic($value,1,50)){
			$this->tipom = $value;
			return true;
		}else{
			return false;
			$this->tipom = $value;
		}
	}
	public function getTipoM(){
		return $this->tipom;
    }

  public function consultarTipoMLimite($empezarDesde,$tamanioPag)
	{
		$sql = "SELECT id_tipom,tipo_movimiento FROM `tipo_movimiento` where estado=1   LIMIT $empezarDesde,$tamanioPag";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function consultarTipoM()
	{
		$sql = "SELECT id_tipom,tipo_movimiento FROM `tipo_movimiento` where estado=1";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	

	public function searchTipoM($value)
	{
		$sql = "SELECT id_tipom,tipo_movimiento FROM `tipo_movimiento` where estado=1 and tipo_movimiento like ? ";
		$params = array("%$value%");
		return Database::getRows($sql, $params);
	}
	public function insertTipoM()
	{
		$sql = "INSERT INTO `tipo_movimiento` (`id_tipom`, `tipo_movimiento`,estado) VALUES (NULL, ?,1)";
		$params = array($this->tipom);	
		return Database::executeRow($sql, $params);
	}
	public function borrarTipoM()
	{
		$sql = "update tipo_movimiento set estado=2 where id_tipom=?";
		$params = array($this->id);	
		return Database::executeRow($sql, $params);
	}
	public function updateTipoM()
	{
		$sql = "UPDATE `tipo_movimiento` SET `tipo_movimiento` =? WHERE id_tipom = ?";
		$params = array($this->tipom,$this->id);	
		return Database::executeRow($sql, $params);
	}
	public function readTipoM()
	{
			$sql    = "SELECT id_tipom,tipo_movimiento FROM `tipo_movimiento` where estado=1 and id_tipom=? ";
			$params = array($this->id);
			$user   = Database::getRow($sql, $params);
			if ($user) {
					$this->tipom   = $user['tipo_movimiento'];
					return true;
			} else {
					return null;
			}
	}


}
?>