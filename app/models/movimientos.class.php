<?php
class Movimientos extends Validator{
	//Declaración de propiedades
	private $id = null;
	private $cantidad = null;
    private $id_producto = null;
    private $fecha = null;
    private $id_tipom = null;
    private $id_estadom = null;
    private $id_procedencia = null;
    private $id_usuario = null;
    private $descripcion=null;
	private $cantivieja=null;
	private $id_bodega=null;
	private $id_estadop=null;
    //Métodos para sobrecarga de propiedades

	public function setId($value){
		if($this->validateId($value)){
			$this->id = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getId(){
		return $this->id;
	}

    public function setDescripcion($value){
		if($this->validateAlphanumeric($value,1,150)){
			$this->descripcion = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getDescripcion(){
		return $this->descripcion;
	}

	public function setCantidad($value){
		if($this->validateAlphanumeric($value, 1, 50)){
			$this->cantidad = $value;
			return true;
		}else{
			return false;
			$this->cantidad= $value;
		}
	}
	public function getCantidad(){
		return $this->cantidad;
    }



	public function setIdTipoM($value){
		if($this->validateId($value)){
			$this->id_tipom = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdTipoM(){
		return $this->id_tipom;
	}
	public function setIdEstadoP($value){
		if($this->validateId($value)){
			$this->id_estadop = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdEstadoP(){
		return $this->id_estadop;
	}
	public function setIdBodega($value){
		if($this->validateId($value)){
			$this->id_bodega = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdBodega(){
		return $this->id_bodega;
    }
    public function setIdProcedencia($value){
		if($this->validateId($value)){
			$this->id_procedencia = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdProcedencia(){
		return $this->id_procedencia;
    }
    public function setIdProducto($value){
		if($this->validateId($value)){
			$this->id_producto = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdProducto(){
		return $this->id_producto;
    }
    public function setIdEstadoM($value){
		if($this->validateId($value)){
			$this->id_estadom = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdEstadoM(){
		return $this->id_estadom;
    }
    public function setIdUsuario($value){
		if($this->validateId($value)){
			$this->id_usuario = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdUsuario(){
		return $this->id_usuario;
    }
    public function setFecha($value){
        if($this->validateAlphanumeric($value,1,60))
        {
             $this->fecha = $value;
                 return true;
        }
        else
        {
            return false;
            
        }
     }
    public function getFecha(){
		return $this->fecha;
	}
	//reportes
	public function getReport1($movimiento,$fecha1,$fecha2){
		$sql = "select producto.codigo,movimiento.usuarioa,usuarios.codigo,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.cantidad,movimiento.fecha from (((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) inner join usuarios on usuarios.id_usuario=movimiento.id_usuario) where movimiento.id_estadom BETWEEN 2 and 3 and movimiento.id_tipom=? and movimiento.fecha between ? and ? order by movimiento.fecha desc";
		$params = array($movimiento,$fecha1,$fecha2);
		return Database::getRows($sql, $params);
	}
	public function getReport3($id){
		$sql = "select producto.codigo,movimiento.usuarioa,usuarios.codigo,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.cantidad,procedencia.procedencia from (((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) inner join usuarios on usuarios.id_usuario=movimiento.id_usuario) where movimiento.id_movimiento=? order by movimiento.fecha desc";
		$params = array($id);
		return Database::getRows($sql, $params);
	}
	public function getReport2(){
		$sql = "select producto.codigo,movimiento.usuarioa,usuarios.codigo,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.cantidad,movimiento.fecha from (((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) inner join usuarios on usuarios.id_usuario=movimiento.id_usuario) where movimiento.id_estadom BETWEEN 2 and 3  order by movimiento.fecha desc";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	//Metodos para el manejo del SCRUD

	public function consultarMovimientosLimite($empezarDesde,$tamanioPag)
	{
		$sql = "select movimiento.id_movimiento, producto.producto,procedencia.procedencia,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.descripcion,movimiento.cantidad,movimiento.fecha from ((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) where movimiento.id_usuario=? and movimiento.id_estadom=1 order by movimiento.fecha	 desc LIMIT $empezarDesde,$tamanioPag";
		$params = array($_SESSION['id']);
		return Database::getRows($sql, $params);
	}
	
	public function consultarIdProd()
	{
		$sql    = "select id_producto,id_tipom,cantidad,id_estadop,id_bodega from movimiento where id_movimiento=? and id_tipom between 1 and 3 ";
			$params = array($this->id);
			$user   = Database::getRow($sql, $params);
			if ($user) {
					$this->id_producto   = $user['id_producto'];
					$this->id_tipom   = $user['id_tipom'];
					$this->cantidad = $user['cantidad'];
					$this->id_estadop = $user['id_estadop'];
					$this->id_bodega = $user['id_bodega'];
					return true;
			} else {
					return null;
			}
	}
	public function searchMovimientosBita($value)
	{
		$sql = "select movimiento.id_movimiento, producto.producto,procedencia.procedencia,movimiento.usuarioa,usuarios.codigo,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.descripcion,movimiento.cantidad,movimiento.fecha from (((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) inner join usuarios on usuarios.id_usuario=movimiento.id_usuario) where movimiento.id_estadom BETWEEN 2 and 3  and producto.producto like ?  order by movimiento.fecha desc";
		$params = array("%$value%");
		return Database::getRows($sql, $params);
	}
	
	public function consultarMovimientosBita()
	{
		$sql = "select movimiento.id_movimiento, producto.producto,procedencia.procedencia,movimiento.usuarioa,usuarios.codigo,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.descripcion,movimiento.cantidad,movimiento.fecha from (((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) inner join usuarios on usuarios.id_usuario=movimiento.id_usuario) where movimiento.id_estadom BETWEEN 2 and 3  order by movimiento.fecha desc";
		$params = array(NULL);
		return Database::getRows($sql, $params);
	}
	public function consultarMovimientosLimiteBita($empezarDesde,$tamanioPag)
	{
		$sql = "select movimiento.id_movimiento, producto.producto,procedencia.procedencia,movimiento.usuarioa,usuarios.codigo,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.descripcion,movimiento.cantidad,movimiento.fecha from (((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) inner join usuarios on usuarios.id_usuario=movimiento.id_usuario) where movimiento.id_estadom BETWEEN 2 and 3  order by movimiento.fecha desc LIMIT $empezarDesde,$tamanioPag";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function searchMovimientosBitaU($value)
	{
		$sql = "select movimiento.id_movimiento, producto.producto,procedencia.procedencia,movimiento.usuarioa,usuarios.codigo,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.descripcion,movimiento.cantidad,movimiento.fecha from (((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) inner join usuarios on usuarios.id_usuario=movimiento.id_usuario) where movimiento.id_estadom BETWEEN 2 and 3 and movimiento.id_usuario=?  and  producto.codigo like ? order by movimiento.fecha desc";
		$params = array($_SESSION['id'],"%$value%");
		return Database::getRows($sql, $params);
	}
	
	public function consultarMovimientosBitaU()
	{
		$sql = "select movimiento.id_movimiento, producto.producto,procedencia.procedencia,movimiento.usuarioa,usuarios.codigo,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.descripcion,movimiento.cantidad,movimiento.fecha from (((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) inner join usuarios on usuarios.id_usuario=movimiento.id_usuario) where movimiento.id_estadom BETWEEN 2 and 3 and movimiento.id_usuario=? order by movimiento.fecha desc";
		$params = array($_SESSION['id']);
		return Database::getRows($sql, $params);
	}
	public function consultarMovimientosLimiteBitaU($empezarDesde,$tamanioPag)
	{
		$sql = "select movimiento.id_movimiento, producto.producto,procedencia.procedencia,movimiento.usuarioa,usuarios.codigo,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.descripcion,movimiento.cantidad,movimiento.fecha from (((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) inner join usuarios on usuarios.id_usuario=movimiento.id_usuario) where movimiento.id_estadom BETWEEN 2 and 3 and movimiento.id_usuario=? order by movimiento.fecha desc LIMIT $empezarDesde,$tamanioPag";
		$params = array($_SESSION['id']);
		return Database::getRows($sql, $params);
	}
	public function searchMovimientos($value)
	{
		$sql = "select movimiento.id_movimiento, producto.producto,procedencia.procedencia,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.descripcion,movimiento.cantidad,movimiento.fecha from ((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) where  movimiento.id_estadom=1  and producto.producto like ? order by estado_movimiento.id_estadom DESC";
		$params = array("%$value%");
		return Database::getRows($sql, $params);
	}
	
	public function consultarMovimientos()
	{
		$sql = "select movimiento.id_movimiento, producto.producto,procedencia.procedencia,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.descripcion,movimiento.cantidad,movimiento.fecha from ((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) where movimiento.id_usuario=? and movimiento.id_estadom=1 order by estado_movimiento.id_estadom DESC";
		$params = array($_SESSION['id']);
		return Database::getRows($sql, $params);
	}
	public function consultarMovimientosLimiteAdmin($empezarDesde,$tamanioPag)
	{
		$sql = "select movimiento.id_movimiento, producto.producto,procedencia.procedencia,usuarios.codigo,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.descripcion,movimiento.cantidad,movimiento.fecha from (((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) inner join usuarios on usuarios.id_usuario=movimiento.id_usuario) where movimiento.id_estadom=1  order by estado_movimiento.id_estadom DESC LIMIT $empezarDesde,$tamanioPag";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	
	public function searchMovimientosAdmin($value)
	{
		$sql = "select movimiento.id_movimiento, producto.producto,procedencia.procedencia,usuarios.codigo,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.descripcion,movimiento.cantidad,movimiento.fecha from (((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) inner join usuarios on usuarios.id_usuario=movimiento.id_usuario) where movimiento.id_estadom=1 and producto.codigo like ? ORDER BY estado_movimiento.id_estadom DESC";
		$params = array("%$value%");
		return Database::getRows($sql, $params);
	}
	
	public function consultarMovimientosAdmin()
	{
		$sql = "select movimiento.id_movimiento, producto.producto,procedencia.procedencia,usuarios.codigo,bodega.bodega,estado_producto.estado_producto,tipo_movimiento.tipo_movimiento,estado_movimiento.estado,movimiento.descripcion,movimiento.cantidad,movimiento.fecha from (((((((movimiento inner join producto on movimiento.id_producto=producto.id_producto) inner join procedencia on procedencia.id_procedencia=movimiento.id_procedencia) inner join bodega on bodega.id_bodega=movimiento.id_bodega) inner join estado_producto on estado_producto.id_estadop=movimiento.id_estadop) inner join tipo_movimiento on tipo_movimiento.id_tipom=movimiento.id_tipom) inner join estado_movimiento on estado_movimiento.id_estadom=movimiento.id_estadom) inner join usuarios on usuarios.id_usuario=movimiento.id_usuario) where movimiento.id_estadom=1  order by estado_movimiento.id_estadom DESC";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
    public function cantiInveMenos(){
		$sql = "select cantidad from inventario where id_producto=? and id_estadop=? and id_bodega=?";
		$params = array($this->id_producto,$this->id_estadop,$this->id_bodega);
		$user = Database::getRow($sql, $params);
		if($user){
			$this->cantivieja= $user['cantidad'];
			$this->cantidad=$this->cantivieja-$this->cantidad;
			return true;
		}else{
			return null;
		}
	}
	public function checkInve(){
		$sql = "select id_inventario from inventario where id_producto=? and id_estadop=? and id_bodega=?";
		$params = array($this->id_producto,$this->id_estadop,$this->id_bodega);
		$user = Database::getRow($sql, $params);
		if($user){
			return true;
		}else{
			return null;
		}
    }
    public function valCanti(){
		if($this->id_tipom==2||$this->id_tipom==3){
            $sql = "select cantidad from inventario where id_producto=? and id_estadop=? and id_bodega=?";
            $params = array($this->id_producto,$this->id_estadop,$this->id_bodega);
            $user = Database::getRow($sql, $params);
            if($user){
                $this->cantivieja= $user['cantidad'];
                if($this->cantivieja<$this->cantidad){
                    return null;
                }else{
                    return true;
                }
            }else{
                return null;
            }
        }else{
            return true;
        }
    }
    public function cantiInveMas(){
		$sql = "select cantidad from inventario where id_producto=? and id_estadop=? and id_bodega=?";
		$params = array($this->id_producto,$this->id_estadop,$this->id_bodega);
		$user = Database::getRow($sql, $params);
		if($user){
			$this->cantivieja= $user['cantidad'];
			$this->cantidad=$this->cantivieja+$this->cantidad;
			return true;
		}else{
			return null;
		}
    }
	public function insertMovimientos(){
        $fecha = date('Y-m-d');
		$sql = "INSERT INTO `movimiento` (`id_movimiento`, `id_producto`, `id_usuario`, `cantidad`, `id_procedencia`, `fecha`, `descripcion`, `usuarioa`, `id_estadom`, `id_tipom`, `id_bodega`, `id_estadop`) VALUES (NULL, ?, ?, ?, ?, ?,? ,NULL,?, ?, ?, ?)";
		$params = array($this->id_producto,$this->id_usuario,$this->cantidad,$this->id_procedencia,$fecha,$this->descripcion,1,$this->id_tipom,$this->id_bodega,$this->id_estadop);
		return Database::executeRow($sql, $params);
    }
    
    public function updateMovimientos($estado){
		$sql = "UPDATE `movimiento` SET `id_estadom`=? , usuarioa= ?  WHERE id_movimiento=?";
		$params = array($estado,$_SESSION['usuario'],$this->id);
		return Database::executeRow($sql, $params);
    }
    public function updateInventario(){
		$sql = "UPDATE `inventario` SET `cantidad`=? WHERE id_producto=? and id_estadop=? and id_bodega=?";
		$params = array($this->cantidad,$this->id_producto,$this->id_estadop,$this->id_bodega);
		return Database::executeRow($sql, $params);
    }
	public function insertInventario(){
		$sql = "INSERT INTO `inventario` (`id_inventario`, `id_producto`, `id_estadop`, `id_bodega`, `cantidad`) VALUES (NULL, ?, ?, ?, ?)";
		$params = array($this->id_producto,$this->id_estadop,$this->id_bodega,$this->cantidad);
		return Database::executeRow($sql, $params);
    }
  
}
?>