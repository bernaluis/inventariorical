<?php
class Procedencia extends Validator{
    private $id=null;
    private $procedencia=null;

    public function setId($value){
		if($this->validateId($value)){
			$this->id = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getId(){
		return $this->id;
    }
    public function setProcedencia($value){
		if($this->validateAlphanumeric($value,1,50)){
			$this->procedencia = $value;
			return true;
		}else{
			return false;
			$this->procedencia= $value;
		}
	}
	public function getProcedencia(){
		return $this->procedencia;
    }

  public function consultarProcedenciaLimite($empezarDesde,$tamanioPag)
	{
		$sql = "SELECT id_procedencia,procedencia FROM `procedencia` where estado=1   LIMIT $empezarDesde,$tamanioPag";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function consultarProcedencia()
	{
		$sql = "SELECT id_procedencia,procedencia FROM `procedencia` where estado=1";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	

	public function searchProcedencia($value)
	{
		$sql = "SELECT id_procedencia,procedencia FROM `procedencia` where estado=1 and procedencia like ? ";
		$params = array("%$value%");
		return Database::getRows($sql, $params);
	}
	public function insertProcedencia()
	{
		$sql = "INSERT INTO `procedencia` (`id_procedencia`, `procedencia`,estado) VALUES (NULL, ?,1)";
		$params = array($this->procedencia);	
		return Database::executeRow($sql, $params);
	}
	public function borrarProcedencia()
	{
		$sql = "update procedencia set estado=2 where id_procedencia=?";
		$params = array($this->id);	
		return Database::executeRow($sql, $params);
	}
	public function updateProcedencia()
	{
		$sql = "UPDATE `procedencia` SET `procedencia` =? WHERE id_procedencia = ?";
		$params = array($this->procedencia,$this->id);	
		return Database::executeRow($sql, $params);
	}
	public function readProcedencia()
	{
			$sql    = "SELECT id_procedencia,procedencia FROM `procedencia` where estado=1 and id_procedencia=? ";
			$params = array($this->id);
			$user   = Database::getRow($sql, $params);
			if ($user) {
					$this->procedencia   = $user['procedencia'];
					return true;
			} else {
					return null;
			}
	}


}
?>