<?php
class Bodega extends Validator{
    private $id=null;
    private $bodega=null;

    public function setId($value){
		if($this->validateId($value)){
			$this->id = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getId(){
		return $this->id;
    }
    public function setBodega($value){
		if($this->validateAlphanumeric($value,1,50)){
			$this->bodega = $value;
			return true;
		}else{
			return false;
			$this->bodega = $value;
		}
	}
	public function getBodega(){
		return $this->bodega;
    }

  public function consultarBodegaLimite($empezarDesde,$tamanioPag)
	{
		$sql = "SELECT id_bodega,bodega FROM `bodega` where estado=1   LIMIT $empezarDesde,$tamanioPag";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function consultarBodega()
	{
		$sql = "SELECT id_bodega,bodega FROM `bodega` where estado=1 ";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	

	public function searchBodega($value)
	{
		$sql = "SELECT id_bodega,bodega FROM `bodega` where estado=1  and bodega like ? ";
		$params = array("%$value%");
		return Database::getRows($sql, $params);
	}
	public function insertBodega()
	{
		$sql = "INSERT INTO `bodega` (`id_bodega`, `bodega`,estado) VALUES (NULL, ?,1)";
		$params = array($this->bodega);	
		return Database::executeRow($sql, $params);
	}
	public function borrarBodega()
	{
		$sql = "update bodega set estado=2 where id_bodega=?";
		$params = array($this->id);	
		return Database::executeRow($sql, $params);
    }
	public function verificarDelete()
	{
		$sql = "SELECT id_inventario FROM `inventario` where  id_bodega=? and cantidad>0";
		$params = array($this->id);
		return Database::getRows($sql, $params);
	}
	public function updateBodega()
	{
		$sql = "UPDATE `bodega` SET `bodega` =? WHERE id_bodega = ?";
		$params = array($this->bodega,$this->id);	
		return Database::executeRow($sql, $params);
	}
	public function readBodega()
	{
		$sql    = "SELECT id_bodega,bodega FROM `bodega` where estado=1 and id_bodega=? ";
		$params = array($this->id);
		$user   = Database::getRow($sql, $params);
		if ($user) {
			$this->bodega   = $user['bodega'];
			return true;
		} else {
			return null;
		}
	}


}
?>