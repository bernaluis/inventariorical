<?php
class Producto extends Validator{
	//Declaración de propiedades
	private $id = null;
	private $codigo = null;
    private $producto = null;
    private $descripcion = null;
    private $id_modelo = null;
    private $id_tipop = null;

    //Métodos para sobrecarga de propiedades

	public function setId($value){
		if($this->validateId($value)){
			$this->id = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getId(){
		return $this->id;
	}
	public function setCodigo($value){
		if($this->validateAlphanumeric($value,1,60)){
			$this->codigo = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getCodigo(){
		return $this->codigo;
	}



	public function setProducto($value){
		if($this->validateAlphanumeric($value, 1, 50)){
			$this->producto = $value;
			return true;
		}else{
			return false;
			$this->producto = $value;
		}
	}
	public function getProducto(){
		return $this->producto;
    }
    public function setDescripcion($value){
		if($this->validateAlphanumeric($value, 1, 50)){
			$this->descripcion = $value;
			return true;
		}else{
			return false;
			$this->descripcion = $value;
		}
	}
	public function getDescripcion(){
		return $this->descripcion;
    }
	public function setIdTipoP($value){
		if($this->validateId($value)){
			$this->id_tipop = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdTipoP(){
		return $this->id_tipop;
	}


	public function setIdModelo($value){
		if($this->validateId($value)){
			$this->id_modelo = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getIdModelo(){
		return $this->id_modelo;
	}
	//Metodos para el manejo del SCRUD

	public function consultarProductoLimite($empezarDesde,$tamanioPag)
	{
		$sql = "select producto.id_producto,producto.codigo,producto.producto,producto.descripcion,modelo.id_modelo,modelo.modelo,marca.id_marca,marca.marca,tipo_producto.id_tipop,tipo_producto.tipo_producto,producto.estado from producto inner join modelo inner join marca inner join tipo_producto where tipo_producto.id_tipop=producto.id_tipop and modelo.id_modelo=producto.id_modelo and modelo.id_marca=marca.id_marca and producto.estado=1 LIMIT $empezarDesde,$tamanioPag";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function readProducto(){
		$sql = "select producto.id_producto,producto.codigo,producto.producto,producto.descripcion,modelo.id_modelo,modelo.modelo,marca.id_marca,marca.marca,tipo_producto.id_tipop,tipo_producto.tipo_producto,producto.estado from producto inner join modelo inner join marca inner join tipo_producto where tipo_producto.id_tipop=producto.id_tipop and modelo.id_modelo=producto.id_modelo and modelo.id_marca=marca.id_marca and producto.estado=1 and producto.id_producto=?";
		$params = array($this->id);
		$user = Database::getRow($sql, $params);
		if($user){
            $this->producto= $user['producto'];
            $this->descripcion= $user['descripcion'];
            $this->id_modelo= $user['id_modelo'];
			$this->id_tipop= $user['id_tipop'];
			$this->codigo=$user['codigo'];
			return true;
		}else{
			return null;
		}
	}
	public function searchProducto($value)
	{
		$sql = "select producto.id_producto,producto.codigo,producto.producto,producto.descripcion,modelo.id_modelo,modelo.modelo,marca.id_marca,marca.marca,tipo_producto.id_tipop,tipo_producto.tipo_producto,producto.estado from producto inner join modelo inner join marca inner join tipo_producto where tipo_producto.id_tipop=producto.id_tipop and modelo.id_modelo=producto.id_modelo and modelo.id_marca=marca.id_marca and producto.estado=1 and producto.codigo like ? ";
		$params = array("%$value%");
		return Database::getRows($sql, $params);
	}
	
	public function consultarProducto()
	{
		$sql = "select producto.id_producto,producto.codigo,producto.producto,producto.descripcion,modelo.id_modelo,modelo.modelo,marca.id_marca,marca.marca,tipo_producto.id_tipop,tipo_producto.tipo_producto,producto.estado from producto inner join modelo inner join marca inner join tipo_producto where tipo_producto.id_tipop=producto.id_tipop and modelo.id_modelo=producto.id_modelo and modelo.id_marca=marca.id_marca and producto.estado=1";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function obtenerID()
	{
		$sql = "select max(id_producto) as maximo from producto where estado=1";
		$params = array(null);
		$user = Database::getRow($sql, $params);
		if($user){
            $this->id= $user['maximo'];
			return true;
		}else{
			return false;
		}
	}
	public function insertInventario()
	{
		$sql = "INSERT INTO `inventario` (`id_inventario`, `id_producto`, `cantidad`) VALUES (NULL, ?, 0)";
		$params = array($this->id);	
		return Database::executeRow($sql, $params);
	}
	public function insertProducto()
	{
		$sql = "INSERT INTO `producto` (`id_producto`,codigo, `producto`, `descripcion`, `id_modelo`, `id_tipop`, `estado`) VALUES (NULL,?, ?, ?, ?, ?, 1)";
		$params = array($this->codigo,$this->producto,$this->descripcion,$this->id_modelo,$this->id_tipop);
		
		return Database::executeRow($sql, $params);
	}

	
	public function updateProducto(){
		$sql = "UPDATE `producto` SET codigo=?, `producto`=?,`descripcion`=?,`id_modelo`=?,`id_tipop`=? WHERE id_producto=?";
		$params = array($this->codigo,$this->producto,$this->descripcion,$this->id_modelo,$this->id_tipop,$this->id);	
		return Database::executeRow($sql, $params);
	}

	public function deleteProducto(){
		$sql = "UPDATE producto SET estado = 2 WHERE id_producto= ?";
		$params = array( $this->id);
		return Database::executeRow($sql, $params);
	}
	public function deleteInventario(){
		$sql = "UPDATE `inventario` SET `cantidad`=0 WHERE id_producto=?";
		$params = array( $this->id);
		return Database::executeRow($sql, $params);
	}
}
?>