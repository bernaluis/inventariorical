<?php
class EstadoP extends Validator{
    private $id=null;
    private $estado=null;

    public function setId($value){
		if($this->validateId($value)){
			$this->id = $value;
			return true;
		}else{
			return false;
		}
	}
	public function getId(){
		return $this->id;
    }
    public function setEstado($value){
		if($this->validateAlphabetic($value,1,50)){
			$this->estado = $value;
			return true;
		}else{
			return false;
			$this->estado = $value;
		}
	}
	public function getEstado(){
		return $this->estado;
    }

  public function consultarEstadoLimite($empezarDesde,$tamanioPag)
	{
		$sql = "SELECT id_estadop,estado_producto FROM `estado_producto` where estado=1   LIMIT $empezarDesde,$tamanioPag";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	public function consultarEstado()
	{
		$sql = "SELECT id_estadop,estado_producto FROM `estado_producto` where estado=1   ";
		$params = array(null);
		return Database::getRows($sql, $params);
	}
	

	public function searchEstado($value)
	{
		$sql = "SELECT id_estadop,estado_producto FROM `estado_producto` where estado=1 and estado_producto like ? ";
		$params = array("%$value%");
		return Database::getRows($sql, $params);
	}
	public function insertEstado()
	{
		$sql = "INSERT INTO `estado_producto` (`id_estadop`, `estado_producto`,estado) VALUES (NULL, ?,1)";
		$params = array($this->estado);	
		return Database::executeRow($sql, $params);
	}
	public function borrarEstado()
	{
		$sql = "update estado_producto set estado=2 where id_estadop=?";
		$params = array($this->id);	
		return Database::executeRow($sql, $params);
    }
	public function updateEstado()
	{
		$sql = "UPDATE `estado_producto` SET `estado_producto` =? WHERE id_estadop = ?";
		$params = array($this->estado,$this->id);	
		return Database::executeRow($sql, $params);
	}
	public function readEstado()
	{
		$sql    = "SELECT id_estadop,estado_producto FROM `estado_producto` where estado=1  and id_estadop=? ";
		$params = array($this->id);
		$user   = Database::getRow($sql, $params);
		if ($user) {
			$this->estado   = $user['estado_producto'];
			return true;
		} else {
			return null;
		}
	}


}
?>