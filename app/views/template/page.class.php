<?php
require_once("../../app/models/database.class.php");
require_once("../../app/helpers/validator.class.php");
require_once("../../app/helpers/component.class.php");
class Page extends Component{
	public static function templateHeader($title){
		session_start();
		ini_set("date.timezone","America/El_Salvador");
		print("
			<!DOCTYPE html>
			<html lang='es'>
			<head>
				<meta charset='utf-8'>
				<title>Dashboard - $title</title>
				<link rel='stylesheet' href='../../web/css/compiled.min.css'>
				<link rel='stylesheet' href='../../web/css/mdb.css'>
				<link rel='stylesheet' href='../../web/css/sweetalert2.min.css'>
				<link rel='stylesheet' href='../../web/css/bootstrap.min.css'>
				<link rel='stylesheet' href='../../web/css/styledashboard.css'>
				<link rel='stylesheet' href='../../web/css/lightbox.css'>
				<link rel='stylesheet' href='../../web/css/select2.min.css'>
				<link rel='stylesheet' href='../../web/css/alertify.min.css'>
				<link rel='stylesheet' href='../../web/css/themes/semantic.min.css'>
				<script type='text/javascript' src='https://www.google.com/jsapi'></script>
    			<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js'></script>
				<script src='../../web/js/sweetalert2.min.js'></script>

				<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
			</head>
			<body class='fixed-sn black-skin'>

		");
		if(isset($_SESSION['usuario'])){
			
			if($_SESSION['tipo']==1)
			{
				print("
				<!--Double navigation-->
				<header>
						 <!-- Sidebar navigation -->
						 <div id='slide-out' class='side-nav sn-bg-4 fixed mdb-sidenav'>
							 <ul class='custom-scrollbar list-unstyled' style='max-height:100vh;'>
								 

								 <li>
								 <hr>
								 <h3 class='flex-center'>Inventario Mantenimiento</h3>
								 <hr>
								 </li>
								 <!--/.Search Form-->
								 <!-- Side navigation links -->
								 <ul class='collapsible collapsible-accordion'>
							 <li><a href='../usuarios' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i> Usuarios</a>
							 </li>
							 <li>
							 <a href='../bitacoramovimientos' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Bicatora de movimientos</a>
						 		</li>

							 <li><a href='../producto' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i> Productos</a>
							 </li>
							 <li><a href='../inventario' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Inventario</a>
							 </li>
							<li>
								<a href='../movimientosadmin' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Movimientos</a>
							</li>
							<li>
								<a href='../tipo_producto' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i> Tipo de producto</a>
							</li>
							<li>
								<a href='../marca' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Marcas</a>
							</li>
							<li>
								<a href='../modelo' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Modelos</a>
							</li>
							<li>
								<a href='../bodega' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Bodegas</a>
							</li>
							<li>
								<a href='../estado_producto' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Estado de producto</a>
							</li>
							<li>
								<a href='../procedencia' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Lugar de procedencia</a>
							</li>
							 <li>
								 <a href='../account/logout.php' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Cerrar sesion</a>
							 </li>
						 </ul>
					 </li>
								 <!--/. Side navigation links -->
							 </ul>
							 <div class='sidenav-bg mask-strong'></div>
						 </div>
						 <!--/. Sidebar navigation -->
						 <!-- Navbar -->
						 <nav class='navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav'>
							 <!-- SideNav slide-out button -->
							 <div class='float-left'>
							 <a href='#' data-activates='slide-out' class='button-collapse'><i class='fa fa-bars'></i></a>
							 </div>
							 <!-- Breadcrumb-->
							 <div class='breadcrumb-dn mr-auto'>
							 
							 </div>
							 <a class=''nav-link waves-light' href='../account/profile.php'><i class='fa fa-male' aria-hidden='true'></i>    Perfil</a>
						 </nav>
						 <!-- /.Navbar -->
					 </header>
				<main>
				");
			}elseif($_SESSION['tipo']==2)
			{
				print("
				<!--Double navigation-->
				<header>
						 <!-- Sidebar navigation -->
						 <div id='slide-out' class='side-nav sn-bg-4 fixed mdb-sidenav'>
							 <ul class='custom-scrollbar list-unstyled' style='max-height:100vh;'>
								 

								 <li>
								 <hr>
								 <h3 class='flex-center'>Inventario Mantenimiento</h3>
								 <hr>
								 </li>
								 <!--/.Search Form-->
								 <!-- Side navigation links -->
								 <ul class='collapsible collapsible-accordion'>
							 


							<li><a href='../producto' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i> Productos</a>
							</li>
							<li><a href='../inventario' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Inventario</a>
							</li>
							<li>
							 <a href='../bitacoramovimientosuser' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Bicatora de movimientos</a>
						 		</li>
							<li>
								<a href='../movimientosuser' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Movimientos</a>
							</li>
							<li>
								<a href='../tipo_producto' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i> Tipo de producto</a>
							</li>
							
							<li>
								<a href='../marca' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Marcas</a>
							</li>
							<li>
								<a href='../modelo' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Modelos</a>
							</li>
							<li>
								<a href='../estado_producto' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Estado de producto</a>
							</li>
							<li>
								<a href='../procedencia' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Lugar de procedencia</a>
							</li>
							 <li>
								 <a href='../account/logout.php' class='collapsible-header waves-effect arrow-r'><i class='fa fa-chevron-right'></i>Cerrar sesion</a>
							 </li>
						 </ul>
					 </li>
								 <!--/. Side navigation links -->
							 </ul>
							 <div class='sidenav-bg mask-strong'></div>
						 </div>
						 <!--/. Sidebar navigation -->
						 <!-- Navbar -->
						 <nav class='navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav'>
							 <!-- SideNav slide-out button -->
							 <div class='float-left'>
							 <a href='#' data-activates='slide-out' class='button-collapse'><i class='fa fa-bars'></i></a>
							 </div>
							 <!-- Breadcrumb-->
							 <div class='breadcrumb-dn mr-auto'>
							 
							 </div>
							 <a class=''nav-link waves-light' href='../account/profile.php'><i class='fa fa-male' aria-hidden='true'></i>    Perfil</a>
						 </nav>
						 <!-- /.Navbar -->
					 </header>
				<main>
				");
					
			}
		}else{
		
			print("
			<!--Double navigation-->
			<header>
				<!-- Sidebar navigation -->
				<div id='slide-out' class='side-nav sn-bg-4 fixed mdb-sidenav'>
					<ul class='custom-scrollbar list-unstyled' style='max-height:100vh;'>
						<!-- Logo -->
						<li>
							<div class='logo-wrapper waves-light'>
								<a href='#'><img src='' class='img-fluid flex-center'></a>
							</div>
						</li>
						<!--/. Logo -->
						<!--Social-->
						<li>
						<hr>
						<h3 class='flex-center'>Inventario Rical</h3>
						<hr>
						</li>
					</ul>
					<div class='sidenav-bg mask-strong'></div>
				</div>
				<!--/. Sidebar navigation -->
				<!-- Navbar -->
				<nav class='navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav'>
					<!-- SideNav slide-out button -->
					<div class='float-left'>
						<a href='#' data-activates='slide-out' class='button-collapse'><i class='fa fa-bars'></i></a>
					</div>
				</nav>
				<!-- /.Navbar -->
			</header>
			<main>
			");
			$filename = basename($_SERVER['PHP_SELF']);
			if($filename != "login.php" && $filename != "register.php"){
				self::showMessage(3, "¡Debe iniciar sesión!", "../account/login.php");
				self::templateFooter();
				exit;
			}
		}
	}

	public static function templateFooter(){
		print("
					</main>
						<!-- ./Fin Main -->
						<script src='../../web/js/jquery-3.2.1.min.js'></script>
						<script type='text/javascript' defer src='../../web/js/fontawesome-all.js'></script>
						<script src='../../web/js/mdb.min.js'></script>
						<script src='../../web/js/compiled.min.js'></script>
						<script src='../../web/js/popper.min.js'></script>
						<script src='../../web/js/sitiodashboard.js'></script>
						<script src='../../web/js/lightbox.js'></script>
						<script src='../../web/js/select2.min.js'></script>
						<!--DateTime-->
						<script src='../../web/js/datetime/picker.date.js'></script>
						<script src='../../web/js/datetime/picker.time.js'></script>
						<script src='../../web/js/datetime/translations/es.js'></script>
				</body>
			</html>
		");
	}
}
?>
