<div class="container-fluid text-center">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="card">
                    <!--Header-->
                    <div class="header pt-3 blue-gradient">
                        <div class="row d-flex justify-content-center">
                            <h3 class="white-text mb-3 pt-3 font-bold">Agregar usuario</h3>
                        </div>
                        <div class="row mt-2 mb-3 d-flex justify-content-center">
                        </div>
                    </div>
                    <!--Header-->
                    <form method='post' autocomplete="off">
                             <div class=" row">
                                <div class=" offset-2 col-8">
                                    <div class="md-form mt-5 mb-4">
                            
                                        <input type="text" id="nombreEncargado" name="nombreEncargado" class="form-control validate" required  title="Ingrese primer y segundo nombre Ej: Juan Perez" value='<?php print($usuario->getNombres()) ?>'>
                                        <label for="nombreEncargado" data-error="Valor incorrecto" data-success="Correcto">Nombres del Usuario</label>
                                    </div>
                                    <div class="md-form mt-5 mb-4">
                            
                                        <input type="text" id="apellidosEncargado" name="apellidosEncargado" class="form-control validate" required  title="Ingrese primer y segundo apellido Ej: Mena Reyes" value='<?php print($usuario->getApellidos()) ?>'>
                                        <label for="apellidosEncargado"  data-error="Valor incorrecto" data-success="Correcto">Apellidos del Usuario</label>
                                    </div>
                                    <div class="md-form mt-5 mb-4">
                            
                                        <input type="text" id="usuarioEncargado" name="usuarioEncargado" class="form-control validate" required pattern="^[a-zA-Z0-9]{8}" title="Ingrese el usuario: Ej letras y/o numeros hasta 8 caracteres" value='<?php print($usuario->getCodigo()) ?>'>
                                        <label for="usuarioEncargado"  data-error="Valor incorrecto" data-success="Correcto">Codigo</label>
                                    </div>
                                    
                                    <div class="md-form mt-5 mb-4">
                                        <input type="email" id="correoEncargado" name="correoEncargado" class="form-control validate" required pattern="^([a-z0-9._-]+)([@]{1})([a-z]+)([.]{1})([com]+)$" title="Ingrese un correo Ej: xxxx@xxxx.com"value='<?php print($usuario->getCorreo()) ?>'>
                                        <label for="correoEncargado"  data-error="Valor incorrecto" data-success="Correcto">Correo electronico:</label>
                                    </div>
                                    <div class="md-form  mt-5 mb-4">
                           
                                        <input type="password" id="contraEncargado" name="contraEncargado" class="form-control validate"  pattern="^[a-zA-Z0-9]+" title="Ingrese la contraseña Ej letras y/o numeros hasta 8 caracteres">
                                        <label for="contraEncargado"  data-error="Valor incorrecto" data-success="Correcto">Contraseña</label>
                                    </div>
                                    
                                    <div class="md-form  mt-5 mb-4">
                            
                                        <input type="password" id="confirmaContra"  name="confirmaContra" class="form-control validate" >
                                        <label for="confirmaContra"  data-error="Valor incorrecto" data-success="Correcto">Confirmar contraseña</label>
                                    </div>
                                    <div class="text-left">
                                        <label>Tipo de usuario:</label>
                                        <select class='js-example-basic-single' name='tipo' id='tipo' style='width: 100%' required>
                                            <option value="1">Administrador</option>
                                            <option value="2">Empleado</option>
                                        </select>
                                    </div>     
                                   
                                    
                                </div>


                            </div>
                            <div class="text-center mt-4">
                                <button class="btn btn-primary  elegant-color" name="crear" id="crear" type="submit">Registrar Usuario</button>
                                <button class="btn btn-primary elegant-color" type="reset">Cancelar</button>
                            </div>
                        </form>
                    </div>
                    <!--/Form with header-->
                </div>
            </div>
        </div>
    </div>
    
