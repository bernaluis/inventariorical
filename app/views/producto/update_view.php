<div class="container-fluid text-center">
    <div class="row justify-content-md-center">
        <div class="col-md-12">
            <div class="card">
                <!--Header-->
                <div class="header pt-3 blue-gradient">
                    <div class="row d-flex justify-content-center">
                        <h3 class="white-text mb-3 pt-3 font-bold">Actualizar Producto</h3>
                    </div>
                    <div class="row mt-2 mb-3 d-flex justify-content-center">
                    </div>
                </div>
                <!--Header-->
                <form method="post" enctype='multipart/form-data'> 
                    <div class=" row">
                        <div class=" offset-2 col-8">
                            <div class="md-form mt-5 mb-4">

                                <input type="text" id="codigo" name="codigo" class="form-control validate" required  title="Ingrese el codigo del producto" value='<?php print($usuario->getCodigo()) ?>'>
                                <label for="codigo" data-error="Valor incorrecto" data-success="Correcto">Codigo del producto</label>
                            </div>
                            <div class="md-form mt-5 mb-4">

                                <input type="text" id="nombre" name="nombre" class="form-control validate" required  title="Ingrese el nombre del producto Ej: Hyper X Fury" value='<?php print($usuario->getProducto()) ?>'>
                                <label for="nombre" data-error="Valor incorrecto" data-success="Correcto">Nombre del producto</label>
                            </div>
                            <div class="md-form mt-5 mb-4">

                                <input type="text" id="descripcion" name="descripcion" class="form-control validate" required  title="Ingrese la descripcion del producto" value='<?php print($usuario->getDescripcion()) ?>'>
                                <label for="descripcion" data-error="Valor incorrecto" data-success="Correcto">Descripcion del producto</label>
                            </div>
                            <div class="text-left">
                                <label for='modelo'>Modelo:</label>
                                    <?php
                                    Page::showSelect("Modelo", "modelo", $modelo->getId(), $modelo->consultarModelo());
                                    ?>
                            </div>
                            <p></p>
                            <div class="text-left">
                                <label for='tipop'>Tipo producto:</label>
                                    <?php
                                    Page::showSelect("Tipo producto", "tipop", $tipop->getId(), $tipop->consultarTipoP());
                                    ?>
                            </div>
                            <p></p>                         
                           
                        </div>
                    </div>
                    <div class="text-center mt-4">
                        <button class="btn btn-primary  elegant-color" name="editar" id="editar" type="submit">Actualizar Producto</button>
                        <button class="btn btn-primary elegant-color" type="reset">Cancelar</button>
                    </div>
                </form>
            </div>
            <!--/Form with header-->
        </div>
    </div>
</div>
</div>