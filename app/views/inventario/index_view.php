<div class="container-fluid text-center"> 
            <div class="row justify-content-md-center">
                <div class="col-md-12">
                    <h3>Inventario</h3>
                </div>
                <div class="col-md-12">
                    <form method='post'>
                    <div class="md-form">
                        <input type="text" id="busqueda" name="busqueda" class="form-control">
                        <label for="busqueda" >Buscar producto</label>
                    </div>
                    <button type='submit' name="buscar" id="buscar" class="btn btn-blue">Buscar producto</button>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-blue" data-toggle="modal" data-target="#modalreporte">
                    Generar reporte
                    </button>
                    </form>
                    <a href="create.php" class='btn btn-large blue'>Agregar inventario</a> 
                </div>
            </div>
            <div class="row justify-content-md-center">
                    <!--Table-->
                    <table class="table table-responsive-ms table-fixed table-hover">
                        
                        <!--Table head-->
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Producto</th>
                                <th>Modelo</th>
                                <th>Marca</th>
                                <th>Bodega</th>
                                <th>Estado</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <!--Table head-->
                        
                        <!--Table body-->
                        <tbody>
                            <?php
                            foreach($data as $row){
                                print("
                                <tr>
                                    <td>$row[codigo]</td>
                                    <td>$row[producto]</td>
                                    <td>$row[modelo]</td>
                                    <td>$row[marca]</td>
                                    <td>$row[bodega]</td>
                                    <td>$row[estado_producto]</td>
                                    <td>$row[cantidad]</td>
                                    <td>
                                        <a href='update.php?id=$row[id_inventario]'  class='btn btn-outline-warning btn-rounded waves-effect'><i class='fa fa-edit'></i></a>
                                        <a href='delete.php?id=$row[id_inventario]' class='btn btn-outline-danger btn-rounded waves-effect'><i class='fa fa-trash'></i></a>
                                    </td>
                                </tr>
                                ");
                            }
                            ?>
                        </tbody>
                        <!--Table body-->

                    </table>
                    <!--Table-->
                    <nav aria-label="pagination example">
                        <ul class="pagination pg-blue">
                            <?php
                                //calcular cantidad de botones
                                for($i=1;$i<=$totalPaginas;$i++)
                                {
                                    print("<li class='page-item'><a class='page-link' href='?pagina=$i'>$i</a></li>");
                                }
                            ?>
                        </ul>
                    </nav>
                </div>
                      
        </div>


<!-- Modal -->
<div class="modal fade" id="modalreporte" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo">Reporte de inventario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
        <form method="post" enctype='multipart/form-data' action="report.php" target="_blank"> 
            <div class="modal-body">
                    <div class=" row">
                        <div class=" offset-2 col-8">
                        <label for='modelo'>Estado del producto:</label>
                                <?php
                                    Page::showSelect("Estado", "estado", $estado->getId(), $estado->consultarEstado());
                                ?>
                                
                            <p></p>
                                                        
                            
                        </div>
                        <div class=" offset-2 col-8">
                            
                            <label for='bodega'>Bodega:</label>
                            <?php
                                Page::showSelect("Bodega", "bodega", $bodega->getId(), $bodega->consultarBodega());
                            ?>
                                
                        <p></p>
                                                        
                            
                    </div>
                            
                </div>
            </div>
                    <div class="modal-footer">
                            <div class="text-center mt-4">
                                <button class="btn btn-primary  elegant-color" name="repog" id="repog" type="submit" value="repog"  target="_blank">Reporte general</button>
                                <button class="btn btn-primary  elegant-color" name="repo" id="repo" type="submit" value="repo"  target="_blank">Generar reporte</button>
                                <button class="btn btn-primary elegant-color" data-dismiss="modal">Cancelar</button>
                            </div>
                    </div>
        </form>
      
      
       
      
    </div>
  </div>
</div>