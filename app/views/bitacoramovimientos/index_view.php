<div class="container-fluid text-center"> 
            <div class="row justify-content-md-center">
                <div class="col-md-12">
                    <h3>Bitacora de Movimientos</h3>
                </div>
                <div class="col-md-12">
                    <form method='post'>
                    <div class="md-form">
                        <input type="text" id="busqueda" name="busqueda" class="form-control">
                        <label for="busqueda" >Buscar movimiento</label>
                    </div>
                    <button type='submit' name="buscar" id="buscar" class="btn btn-blue">Buscar movimiento</button>
                     <!-- Button trigger modal -->
                     <button type="button" class="btn btn-blue" data-toggle="modal" data-target="#modalreporte">
                    Generar reporte
                    </button>
                    </form>
                    
                </div>
            </div>
            <div class="row justify-content-md-center">
                    
                    <table class="table table-responsive-ms table-fixed table-hover">
                        
                        <!--Table head-->
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Usuario</th>
                                <th>Lugar</th>
                                <th>Fecha</th>
                                <th>Descripcion</th>
                                <th>Estado</th>
                                <th>Tipo</th>
                                <th>Administrador</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>

                        <!--Table head-->
                        
                        <!--Table body-->
                        <tbody>
                            <?php
                            foreach($data as $row){
                                print("
                                <tr>
                                    <td>$row[producto]</td>
                                    <td>$row[cantidad]</td>
                                    <td>$row[codigo]</td>
                                    <td>$row[procedencia]</td>
                                    <td>$row[fecha]</td>
                                    <td>$row[descripcion]</td>
                                    <td>$row[estado]</td>
                                    <td>$row[tipo_movimiento]</td>
                                    <td>$row[usuarioa]</td>
                                    <td>
                                        <a href='report2.php?id=$row[id_movimiento]'  class='btn btn-outline-warning btn-rounded waves-effect'><i class='fa fa-edit'></i></a>
                                        
                                    </td>
                                </tr>
                                ");
                            }
                            ?>
                        </tbody>
                        <!--Table body-->

                    </table>
                    <!--Table-->
                    <nav aria-label="pagination example">
                        <ul class="pagination pg-blue">
                            <?php
                                //calcular cantidad de botones
                                for($i=1;$i<=$totalPaginas;$i++)
                                {
                                    print("<li class='page-item'><a class='page-link' href='?pagina=$i'>$i</a></li>");
                                }
                            ?>
                        </ul>
                    </nav>
                </div>
                      
        </div>
<!-- Modal -->
<div class="modal fade" id="modalreporte" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo">Reporte de movimientos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
        <form method="post" enctype='multipart/form-data' action="report.php" target="_blank"> 
            <div class="modal-body">
                    <div class=" row">
                        <div class=" offset-2 col-8">
                        <label for='movimiento'>Tipo de movimiento:</label>
                                <?php
                                    Page::showSelect("Movimiento", "movimiento", $movimiento->getId(), $movimiento->consultarTipoM());
                                ?>
                                
                            <p></p>
                                                        
                            
                        </div>
                        <div class=" offset-2 col-8">
                            <div class="md-form mt-5 mb-4">
                                <p>Fecha de inicio:</p>
                                <input type="date" id="fecha1" name="fecha1" class="form-control">
                                
                            </div>
                                                    
                           
                        </div>
                        <div class=" offset-2 col-8">
                            <div class="md-form mt-5 mb-4">
                                <p>Fecha de fin:</p>
                                <input type="date" id="fecha2" name="fecha2" class="form-control" >
                                
                            </div>
                                                    
                           
                        </div>
                            
                </div>
            </div>
                    <div class="modal-footer">
                            <div class="text-center mt-4">
                                <button class="btn btn-primary  elegant-color" name="repog" id="repog" type="submit" value="repog"  target="_blank">Reporte general</button>
                                <button class="btn btn-primary  elegant-color" name="repo" id="repo" type="submit" value="repo"  target="_blank">Generar reporte</button>
                                <button class="btn btn-primary elegant-color" data-dismiss="modal">Cancelar</button>
                            </div>
                    </div>
        </form>
      
      
       
      
    </div>
  </div>
</div>