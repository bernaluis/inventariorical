<div class="container-fluid text-center"> 
            <div class="row justify-content-md-center">
                <div class="col-md-12">
                    <h3>Bitacora de Movimientos</h3>
                </div>
                <div class="col-md-12">
                    <form method='post'>
                    <div class="md-form">
                        <input type="text" id="busqueda" name="busqueda" class="form-control">
                        <label for="busqueda" >Buscar movimiento</label>
                    </div>
                    <button type='submit' name="buscar" id="buscar" class="btn btn-blue">Buscar movimiento</button>
                    </form>
                    
                </div>
            </div>
            <div class="row justify-content-md-center">
                    
                    <table class="table table-responsive-ms table-fixed table-hover">
                        
                        <!--Table head-->
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Administrador</th>
                                <th>Lugar</th>
                                <th>Fecha</th>
                                <th>Descripcion</th>
                                <th>Estado</th>
                                <th>Tipo</th>
                            </tr>
                        </thead>

                        <!--Table head-->
                        
                        <!--Table body-->
                        <tbody>
                            <?php
                            foreach($data as $row){
                                print("
                                <tr>
                                    <td>$row[producto]</td>
                                    <td>$row[cantidad]</td>
                                    <td>$row[usuarioa]</td>
                                    <td>$row[procedencia]</td>
                                    <td>$row[fecha]</td>
                                    <td>$row[descripcion]</td>
                                    <td>$row[estado]</td>
                                    <td>$row[tipo_movimiento]</td>
                                    
                                </tr>
                                ");
                            }
                            ?>
                        </tbody>
                        <!--Table body-->

                    </table>
                    <!--Table-->
                    <nav aria-label="pagination example">
                        <ul class="pagination pg-blue">
                            <?php
                                //calcular cantidad de botones
                                for($i=1;$i<=$totalPaginas;$i++)
                                {
                                    print("<li class='page-item'><a class='page-link' href='?pagina=$i'>$i</a></li>");
                                }
                            ?>
                        </ul>
                    </nav>
                </div>
                      
        </div>
