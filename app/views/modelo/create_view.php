<div class="container-fluid text-center">
    <div class="row justify-content-md-center">
        <div class="col-md-12">
            <div class="card">
                <!--Header-->
                <div class="header pt-3 blue-gradient">
                    <div class="row d-flex justify-content-center">
                        <h3 class="white-text mb-3 pt-3 font-bold">Agregar Modelo</h3>
                    </div>
                    <div class="row mt-2 mb-3 d-flex justify-content-center">
                    </div>
                </div>
                <!--Header-->
                <form method="post" enctype='multipart/form-data'> 
                    <div class=" row">
                        <div class=" offset-2 col-8">
                            <div class="md-form mt-5 mb-4">

                                <input type="text" id="nombre" name="nombre" class="form-control validate" required  title="Ingrese el nombre del modelo Ej: Hyper X Fury" value='<?php print($usuario->getModelo()) ?>'>
                                <label for="nombre" data-error="Valor incorrecto" data-success="Correcto">Nombre del modelo</label>
                            </div>
                            <div class="text-left">
                                <label for='marca'>Marca:</label>
                                    <?php
                                    Page::showSelect("Marca", "marca", $marca->getId(), $marca->consultarMarca());
                                    ?>
                            </div>
                            <p></p>                        
                           
                        </div>
                    </div>
                    <div class="text-center mt-4">
                        <button class="btn btn-primary  elegant-color" name="crear" id="crear" type="submit">Registrar Modelo</button>
                        <button class="btn btn-primary elegant-color" type="reset">Cancelar</button>
                    </div>
                </form>
            </div>
            <!--/Form with header-->
        </div>
    </div>
</div>
</div>