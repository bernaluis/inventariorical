<div class="container-fluid text-center"> 
            <div class="row justify-content-md-center">
                <div class="col-md-12">
                    <h3>Modelos</h3>
                </div>
                <div class="col-md-12">
                    <form method='post'>
                    <div class="md-form">
                        <input type="text" id="busqueda" name="busqueda" class="form-control">
                        <label for="busqueda" >Buscar Modelo</label>
                    </div>
                    <button type='submit' name="buscar" id="buscar" class="btn btn-blue">Buscar Modelo</button>
                    </form>
                    <a href="create.php" class='btn btn-large blue'>Agregar Modelo</a> 
                </div>
            </div>
            <div class="row justify-content-md-center">
                    <!--Table-->
                    <table class="table table-responsive-ms table-fixed table-hover">
                        
                        <!--Table head-->
                        <thead>
                            <tr>
                                <th>Modelo</th>
                                <th>Marca</th>
                            </tr>
                        </thead>
                        <!--Table head-->
                        
                        <!--Table body-->
                        <tbody>
                            <?php
                            foreach($data as $row){
                                print("
                                <tr>
                                    <td>$row[modelo]</td>
                                    <td>$row[marca]</td>
                                    <td>
                                        <a href='update.php?id=$row[id_modelo]'  class='btn btn-outline-warning btn-rounded waves-effect'><i class='fa fa-edit'></i></a>
                                        <a href='delete.php?id=$row[id_modelo]' class='btn btn-outline-danger btn-rounded waves-effect'><i class='fa fa-trash'></i></a>
                                    </td>
                                </tr>
                                ");
                            }
                            ?>
                        </tbody>
                        <!--Table body-->

                    </table>
                    <!--Table-->
                    <nav aria-label="pagination example">
                        <ul class="pagination pg-blue">
                            <?php
                                //calcular cantidad de botones
                                for($i=1;$i<=$totalPaginas;$i++)
                                {
                                    print("<li class='page-item'><a class='page-link' href='?pagina=$i'>$i</a></li>");
                                }
                            ?>
                        </ul>
                    </nav>
                </div>
                      
        </div>
