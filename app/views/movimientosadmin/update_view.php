<div class="container-fluid text-center">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="card">
                    <!--Header-->
                    <div class="header pt-3 red">
                        <div class="row d-flex justify-content-center">
                            <h3 class="white-text mb-3 pt-3 font-bold">Aprobar solicitud</h3>
                        </div>
                        <div class="row mt-2 mb-3 d-flex justify-content-center">
                        </div>
                    </div>
                    <!--Header-->
                    <div class="card-body mx-4 mt-4">
                        <form method="post" autocomplete="off" target="_blank"> 
                        <h3>Estas a punto de aprobar esta solicitud. ¿Estas seguro?</h3>
                            <div class="justify-content-center">
                                <button type="submit" name="edit" id="edit"  class="btn btn-elegant">Confirmar</button>
                            </div>
                            <div class="justify-content-center">
                                <a href="index.php"  class="btn btn-red">Regresar</a>
                            </div>
                        </form>
                    </div>
                    <!--/Form with header-->
                </div>
            </div>
        </div>
    </div>
    