<div class="container-fluid text-center">
            <section class="form-gradient">
            <!--Formulario con header-->
                <div class="card">
                <!--Header-->
                    <div class="header pt-3 blue-gradient">
                        <div class="row d-flex justify-content-center">
                            <h3 class="white-text mb-3 pt-3 font-bold">Iniciar Sesion</h3>
                        </div>
                        <div class="row mt-2 mb-3 d-flex justify-content-center">
                        </div>
                    </div>
                    <!--Header-->
                    <div class="card-body mx-4 mt-4">
                        <form method='POST' autocomplete="off">
                        <!--Elementos del formulario-->
                        <div class="md-form">
                            <input type="text" id="alias" name="alias" class="form-control" value='<?php print($object->getCodigo()) ?>' required/>
                            <label for="alias">Ingrese el alias:</label>
                        </div>
                        <div class="md-form">
                            <input type="password" id="contra" name="contra" class="form-control"  value='<?php print($object->getContra()) ?>' required/>
                            <label for="contra">Ingrese la contraseña</label>
                            <p class="font-small grey-text d-flex justify-content-end"><a data-toggle="modal" data-target="#recuperarModal" class="dark-grey-text ml-1 font-bold">Olvidaste tu contraseña?</a></p>
                        </div>
                        
                        <!--Grid row-->
                        <div class="row d-flex align-items-center mb-1">
                            <!--Grid column-->
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button type="submit" name="sesion" id="sesion" class="btn btn-elegant btn-rounded z-depth-1a">Iniciar Sesion</button>
                                </div>
                            </div>
                            <!--Grid column-->
                        </div>
                        <!--Grid row-->
                        </form>
                        </div>
                        <!--/Form with header-->

                    </section>
        
        </div>