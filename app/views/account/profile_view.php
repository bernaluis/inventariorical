<div class="container-fluid text-center">

    <form method="POST" autocomplete="off">
        <!-- Formulario-->
        <div class="row justify-content-md-center">
            <!--Nombre-->
            <div class="md-form col-sm-12 col-md-6">
                <input type="text" id="nombre" name="nombre" class="form-control" value='<?php print($usuario->getNombres()) ?>' required  pattern="^[a-zA-Z ]+$" title="Ingrese primer y segundo nombre Ej: Juan Perez"/>
                <label for="nombre">Nombre del usuario:</label>
            </div>
            <!--Apellido-->
            <div class="md-form col-sm-12 col-md-6">
                <input type="text" id="apellido" name="apellido" class="form-control" value='<?php print($usuario->getApellidos()) ?>' required pattern="^[a-zA-Z ]+$" title="Ingrese primer y segundo apellido Ej: Mena Reyes"/>
                <label for="apellido">Apellido del usuario:</label>
            </div>
            <!--Alias-->
            <div class="md-form col-sm-12 col-md-6">
                <input type="text" id="alias" name="alias" class="form-control" value='<?php print($usuario->getCodigo()) ?>' required pattern="^[a-zA-Z0-9]{8}" title="Ingrese el usuario: Ej letras y/o numeros hasta 8 caracteres"/>
                <label for="apellido">Alias del usuario:</label>
            </div>
            <!--Correo Electronico-->
            <div class="md-form col-sm-12 col-md-6">
                <input type="email" id="correo" name="correo" class="form-control" value='<?php print($usuario->getCorreo()) ?>' required pattern="^([a-z0-9._-]+)([@]{1})([a-z]+)([.]{1})([com]+)$" title="Ingrese un correo Ej: xxxx@xxxx.com"/>
                <label for="correo">Correo del usuario:</label>
            </div>
           
            <div class="col-sm-12 col-md-12">
                <button class="btn btn-blue" id="editar" name="editar">
                    <i class="fa fa-edit mr-1"></i>Modificar</button>
            </div>
        </div>
        <!-- Grid row -->
    </form>
</div>