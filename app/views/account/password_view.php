<div class="container-fluid text-center">
    <form method="post" autocomplete="off">
        <hr>
        <div class="row justify-content-md-center">
        <div class="col-sm-12 col-md-12">
            <h3 class="text-center">Cambiar contraseña</h3>
        </div>
        <!--Contra Nueva 1-->
        <div class="md-form col-sm-12 col-md-12">
            <input type="password" id="contrauno" name="contrauno" class="form-control" required/>
        <label for="contrauno">Nueva contraseña:</label>
        <!--Contra Nueva 2-->
        <div class="md-form col-sm-12 col-md-12">
            <input type="password" id="contrados" name="contrados" class="form-control" required/>
            <label for="contrados">Confirmar contraseña:</label>
        </div>
        <!--Contra Anterior-->
        <div class="md-form col-sm-12 col-md-12">
            <input type="password" id="contraanterior" name="contraanterior" class="form-control" aria-describedby="ayudaContra" required/>
            <label for="contraanterior">Contraseña anterior:</label>
            <small id="ayudaContra" class="form-text text-muted">
            Para poder agregar la contraseña nueva debe agregar la contraseña anterior
            </small>
        </div>
        <div class="col-sm-12 col-md-12">
            <button class="btn btn-blue" id="modiContra" name="modiContra"><i class="fa fa-edit mr-1"></i>Modificar Contraseña</button>
        </div>
        </div>
    </form>
</div>