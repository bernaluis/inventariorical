<!--Modal Recuperar Contraseña-->
<div class="modal fade show" id="recuperarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notify modal-info modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="heading lead" id="exampleModalLabel">Olvidaste tu contraseña?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">×</span>
                </button>
            </div>
            <form method="post" autocomplete="off">
                <div class="modal-body">
                    <div>
                        <blockquote>
                            <p class="text-left">Si has perdido tu contraseña ingresa tu usuario y se te enviara un mensaje a tu correo con una nueva contraseña</p>
                        </blockquote>
                    </div>
                    <div class="md-form">
                        <input type="text" id="alias2" name="alias2" class="form-control validate">
                        <label for="alias2" data-error="Valor incorrecto" data-success="Correcto">Ingrese el usuario:</label>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="submit" name="recuperar" id="recuperar" class="btn btn-elegant">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>