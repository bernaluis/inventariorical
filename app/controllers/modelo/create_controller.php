<?php
require_once("../../app/models/modelo.class.php");
require_once("../../app/models/marca.class.php");
try{
    $usuario = new Modelo;
    $marca=new Marca;
    if(isset($_POST['crear'])){
        $_POST = $usuario->validateForm($_POST);
        if($usuario->setModelo($_POST['nombre'])){
            if($usuario->setIdMarca($_POST['marca'])){
                if($usuario->insertModelo()){
                    Page::showMessage(1, "Modelo registrado", "index.php");
                }else{
                    
                    throw new Exception("No se pudo guardar el modelo, intentelo mas tarde");
                }
            }else{
                throw new Exception("Marca incorrecta");
            }            
        }else{
            throw new Exception("Nombre incorrecto");
        }        
    }
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
    require_once("../../app/views/modelo/create_view.php");
}
require_once("../../app/views/modelo/create_view.php");
?>