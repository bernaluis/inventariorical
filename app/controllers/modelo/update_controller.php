<?php
require_once("../../app/models/modelo.class.php");
require_once("../../app/models/marca.class.php");
try{
    $usuario = new Modelo;
    $marca=new Marca;
    if($usuario->setId($_GET['id']))
   {
        if($usuario->readModelo())
        {
            if(isset($_POST['editar'])){
                $_POST = $usuario->validateForm($_POST);
                if($usuario->setModelo($_POST['nombre'])){
                    if($usuario->setIdMarca($_POST['marca'])){
                        if($usuario->updateModelo()){
                            Page::showMessage(1, "Modelo actualizado", "index.php");
                        }else{
                            
                            throw new Exception("No se pudo actualizar el modelo, intentelo mas tarde");
                        }
                    }else{
                        throw new Exception("Modelo incorrecta");
                    }            
                }else{
                    Page::showMessage(2,"Seleccione un modelo","index.php");
                }        
            }
        }else{
            throw new Exception("Error");
        }
   }else{
    Page::showMessage(2,"Seleccione un modelo","index.php");
   } 
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
    require_once("../../app/views/modelo/update_view.php");
}
require_once("../../app/views/modelo/update_view.php");
?>