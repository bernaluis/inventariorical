<?php
require_once("../../app/models/modelo.class.php");
try{
	if(isset($_GET['id'])){
		$usuario = new Modelo;
		if($usuario->setId($_GET['id'])){
			
				if(isset($_POST['eliminar'])){
					if($usuario->deleteModelo()){
						Page::showMessage(1, "Modelo eliminado", "index.php");
					}else{
						throw new Exception(Database::getException());
					}
				}
			
		}else{
			throw new Exception("Modelo incorrecto");
		}
	}else{
		Page::showMessage(3, "Seleccione un modelo", "index.php");
	}
}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "index.php");
}
require_once("../../app/views/modelo/delete_view.php");
?>