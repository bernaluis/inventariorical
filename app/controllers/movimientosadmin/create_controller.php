<?php
require_once("../../app/models/movimientos.class.php");
try{
	if(isset($_GET['id'])){
		$usuario = new Movimientos;
		if($usuario->setId($_GET['id'])){
			if(isset($_POST['edit'])){
				if($usuario->updateMovimientos(2)){
					if($usuario->consultarIdProd()){
						
						if($usuario->getIdTipoM()==1){
							if($usuario->checkInve()){
								if($usuario->cantiInveMas()){
									if($usuario->updateInventario()){
										Page::showMessage(1, "Solicitud aprobada", "index.php");
									}else{
										throw new Exception("No se pudo modificar el inventario");	
									}
								}else{
									throw new Exception("No se pudo modificar el inventario");	
								}
							}else{
								if($usuario->insertInventario()){
									Page::showMessage(1, "Solicitud aprobada, se creo un nuevo inventario", "index.php");
								}else{
									throw new Exception("No se pudo modificar el inventario");	
								}
							}
						}else{
							if($usuario->cantiInveMenos()){
								if($usuario->insertInventario()){
									Page::showMessage(1, "Solicitud aprobada", "index.php");
								}else{
									throw new Exception("No se pudo modificar el inventario");	
								}
							}else{
								throw new Exception("No se pudo modificar el inventario");	
							}
						}
					}else{
						throw new Exception("Error al modificar el inventario, verificar si el producto aun existe o si la solicitud fue borrada");
					}
				}else{
					throw new Exception(Database::getException());
				}
			}
		}else{
			throw new Exception("Error");	
		}
	}else{
		Page::showMessage(3, "Seleccione una solicitud", "index.php");
	}
}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "index.php");
}
require_once("../../app/views/movimientosadmin/update_view.php");

?>