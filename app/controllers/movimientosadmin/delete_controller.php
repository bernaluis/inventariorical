<?php
require_once("../../app/models/movimientos.class.php");
try{
	if(isset($_GET['id'])){
		$usuario = new Movimientos;
		if($usuario->setId($_GET['id'])){
			if(isset($_POST['eliminar'])){
				if($usuario->updateMovimientos(3)){
					Page::showMessage(1, "Solicitud rechazada", "index.php");
				}else{
					throw new Exception(Database::getException());
				}
			}
		}else{
			throw new Exception("Error");	
		}
	}else{
		Page::showMessage(3, "Seleccione una solicitud", "index.php");
	}
}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "index.php");
}
require_once("../../app/views/movimientosadmin/delete_view.php");
?>