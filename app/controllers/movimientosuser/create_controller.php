<?php
require_once("../../app/models/procedencia.class.php");
require_once("../../app/models/tipo_movimiento.class.php");
require_once("../../app/models/producto.class.php");
require_once("../../app/models/movimientos.class.php");
require_once("../../app/models/bodega.class.php");
require_once("../../app/models/estado_producto.class.php");
try{
    $user=new Movimientos;
    $procedencia=new Procedencia;
    $tipom=new TipoMovimiento;
    $producto=new Producto;
    $bodega=new Bodega;
    $estado=new EstadoP;
    if(isset($_POST['crear'])){
        $_POST = $user->validateForm($_POST);
        if($user->setIdProducto($_POST['producto'])){
            if($user->setDescripcion($_POST['descripcion'])){
                if($user->setCantidad($_POST['cantidad'])){
                    if($user->setIdProcedencia($_POST['procedencia'])){
                        if($user->setIdTipoM($_POST['tipom'])){
                            if($user->setIdUsuario($_SESSION['id'])){
                                if($user->setIdEstadoP($_POST['estado'])){
                                    if($user->setIdBodega($_POST['bodega'])){
                                        if($user->valCanti()){
                                            if($user->insertMovimientos()){
                                                Page::showMessage(1, "Solicitud generada", "index.php");
                                            }else{
                                                  throw new Exception("No se pudo registrar la solicitud, intentelo mas tarde");
                                            }
                                        }else{
                                            Page::showMessage(1, "La cantidad excede al inventario para el proceso a realizar/El producto no posee un inventario en esa bodega/estado", "create.php");
                                            
                                        }
                                    }else{
                                        throw new Exception("Bodega incorrecta");
                                    }
                                }else{
                                    throw new Exception("Estado de producto incorrecto");
                                }
                            }else{
                                throw new Exception("Usuario incorrecto");
                            }
                        }else{
                            throw new Exception("Seleccione el tipo de movimiento");
                        }
                    }else{
                        throw new Exception("Seleccione la procedencia/destino");
                    }
                }else{
                    throw new Exception("Ingrese la cantidad");
                }
            }else{
                throw new Exception("Ingrese la descripcion");
            }
        }else{
            throw new Exception("Seleccione un producto");
        }
    }        
    
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
    require_once("../../app/views/movimientosuser/create_view.php");
}
require_once("../../app/views/movimientosuser/create_view.php");
?>
