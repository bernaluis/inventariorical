<?php
require_once("../../app/models/inventario.class.php");
require_once("../../app/models/producto.class.php");
require_once("../../app/models/bodega.class.php");
require_once("../../app/models/estado_producto.class.php");
try{
    $usuario = new Inventario;
    $producto=new Producto;
    $bodega=new Bodega;
    $estado=new EstadoP;
    if(isset($_POST['crear'])){
        $_POST = $usuario->validateForm($_POST);
        if($usuario->setCantidad($_POST['cantidad'])){
            if($usuario->setIdProducto($_POST['producto'])){
                if($usuario->setIdBodega($_POST['bodega'])){
                    if($usuario->setIdEstado($_POST['estado'])){
                        if($usuario->insertInventario()){
                            Page::showMessage(1, "Inventario actualizado", "index.php");
                        }else{
                            
                            throw new Exception("No se pudo actualizar el inventario, intentelo mas tarde");
                        }
                    }else{
                        throw new Exception("Producto incorrecto");
                    }            
                }else{
                    throw new Exception("Producto incorrecto");
                }            
            }else{
                throw new Exception("Producto incorrecto");
            }            
        }else{
            Page::showMessage(2,"Seleccione un inventario","index.php");
        }        
    }
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
    require_once("../../app/views/inventario/create_view.php");
}
require_once("../../app/views/inventario/create_view.php");
?>
