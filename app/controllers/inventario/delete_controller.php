<?php
require_once("../../app/models/inventario.class.php");
try{
	if(isset($_GET['id'])){
		$usuario = new Inventario;
		if($usuario->setId($_GET['id'])){
			
				if(isset($_POST['eliminar'])){
					if($usuario->deleteInventario()){
						Page::showMessage(1, "Inventario eliminado", "index.php");
					}else{
						throw new Exception(Database::getException());
					}
				}
		
			
		}else{
			throw new Exception("Error");	
		}
	}else{
		Page::showMessage(3, "Seleccione un inventario", "index.php");
	}
}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "index.php");
}
require_once("../../app/views/inventario/delete_view.php");
?>