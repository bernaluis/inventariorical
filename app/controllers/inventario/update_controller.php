<?php
require_once("../../app/models/inventario.class.php");
require_once("../../app/models/producto.class.php");
try{
    $usuario = new Inventario;
    $producto=new Producto;
    if($usuario->setId($_GET['id']))
   {
        if($usuario->readInventario())
        {
            if(isset($_POST['editar'])){
                $_POST = $usuario->validateForm($_POST);
                if($usuario->setCantidad($_POST['cantidad'])){
                    
                        if($usuario->updateInventario()){
                            Page::showMessage(1, "Inventario actualizado", "index.php");
                        }else{
                            
                            throw new Exception("No se pudo actualizar el inventario, intentelo mas tarde");
                        }
                        
                }else{
                    Page::showMessage(2,"Seleccione un inventario","index.php");
                }        
            }
        }else{
            throw new Exception("Error");
        }
   }else{
    Page::showMessage(2,"Seleccione un inventario","index.php");
   } 
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
    require_once("../../app/views/inventario/update_view.php");
}
require_once("../../app/views/inventario/update_view.php");
?>