<?php

include($_SERVER['DOCUMENT_ROOT'] . '/inventariorical/app/controllers/plantilla.php');;
require_once($_SERVER['DOCUMENT_ROOT'] . '/inventariorical/app/Libraries/fpdf/fpdf.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/inventariorical/app/models/inventario.class.php');


$title = 'Reporte de inventario';
$user=new Inventario();
$pdf   = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetMargins(5 , 25, 5); // Margenes izq y derc, arriba 
try{
    if(isset($_POST['repo'])){
        if(isset($_POST['bodega'])){
            if(isset($_POST['estado'])){
                
                $pdf->SetFont('Helvetica','',11);
                
                
                $pdf->Cell(0,10,utf8_decode('Reporte de inventario filtrado por bodega y estado de producto'),0,1,'C');
                $data1 = $user->getReport1($_POST['bodega'],$_POST['estado']);
                
                if($data1){
                    //tabla 1
                    $pdf->Ln();
                    
                    
                    $pdf->SetFont('Helvetica','B',8); // Aplicamos tipo Bold
                    $header1 = array('Codigo','Estado','Bodega','Cantidad','Producto','Modelo','Marca');
                    $pdf->TablaInve($header1,$data1);

                    
                }else{
                    throw new Exception('No hay datos disponibles');
                }
            }else{
                $pdf->SetFont('Helvetica','',11);
                
                
                $pdf->Cell(0,10,utf8_decode('Reporte de inventario filtrado por bodega'),0,1,'C');
                $data1 = $user->getReport3($_POST['bodega']);
                
                if($data1){
                    //tabla 1
                    $pdf->Ln();
                    
                    
                    $pdf->SetFont('Helvetica','B',8); // Aplicamos tipo Bold
                    $header1 = array('Codigo','Estado','Bodega','Cantidad','Producto','Modelo','Marca');
                    $pdf->TablaInve($header1,$data1);

                    
                }else{
                    throw new Exception('No hay datos disponibles');
                }
            }
        }else{
            $pdf->SetFont('Helvetica','',11);
                
                
                $pdf->Cell(0,10,utf8_decode('Reporte de inventario filtrado por  estado de producto'),0,1,'C');
                $data1 = $user->getReport2($_POST['estado']);
                
                if($data1){
                    //tabla 1
                    $pdf->Ln();
                    
                    
                    $pdf->SetFont('Helvetica','B',8); // Aplicamos tipo Bold
                    $header1 = array('Codigo','Estado','Bodega','Cantidad','Producto','Modelo','Marca');
                    $pdf->TablaInve($header1,$data1);

                    
                }else{
                    throw new Exception('No hay datos disponibles');
                }
        }
    }else if(isset($_POST['repog'])){
        $pdf->SetFont('Helvetica','',11);
                
                
                $pdf->Cell(0,10,utf8_decode('Reporte de inventario general'),0,1,'C');
                $data1 = $user->getReport4($_POST['bodega'],$_POST['estado']);
                
                if($data1){
                    //tabla 1
                    $pdf->Ln();
                    
                    
                    $pdf->SetFont('Helvetica','B',8); // Aplicamos tipo Bold
                    $header1 = array('Codigo','Estado','Bodega','Cantidad','Producto','Modelo','Marca');
                    $pdf->TablaInve($header1,$data1);

                    
                }else{
                    throw new Exception('No hay datos disponibles');
                }
    }else{
        throw new Exception('Seleccione un filtro');
    }

}catch(Exception $error){
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(17, 10, $error->getMessage(), 0, 'L');
}
$pdf->Output();