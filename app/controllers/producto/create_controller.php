<?php
require_once("../../app/models/producto.class.php");
require_once("../../app/models/modelo.class.php");
require_once("../../app/models/tipo_producto.class.php");
try{
    $usuario = new Producto;
    $modelo=new Modelo;
    $tipop=new TipoProducto;
    if(isset($_POST['crear'])){
        $_POST = $usuario->validateForm($_POST);
        if($usuario->setCodigo($_POST['codigo'])){
            if($usuario->setProducto($_POST['nombre'])){
                if($usuario->setDescripcion($_POST['descripcion'])){
                    if($usuario->setIdModelo($_POST['modelo'])){
                        if($usuario->setIdTipoP($_POST['tipop'])){
                            if($usuario->insertProducto()){
                                if($usuario->obtenerID()){
                                    Page::showMessage(1, "Producto registrado", "index.php");
                                }else{
                                    
                                    throw new Exception("No se pudo generar el inventario, intentelo mas tarde");
                                }
                            }else{
                                
                                throw new Exception("No se pudo guardar el producto, intentelo mas tarde");
                            }
                        }else{
                            throw new Exception("Tipo de producto incorrecto");
                        }   
                    }else{
                        throw new Exception("Modelo incorrecto");
                    }   
                }else{
                    throw new Exception("Descripcion incorrecta");
                }            
            }else{
                throw new Exception("Nombre incorrecto");
            }   
        }else{
            throw new Exception("Codigo incorrecto");
        }     
    }
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
    require_once("../../app/views/producto/create_view.php");
}
require_once("../../app/views/producto/create_view.php");
?>

