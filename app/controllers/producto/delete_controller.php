<?php
require_once("../../app/models/producto.class.php");
try{
	if(isset($_GET['id'])){
		$usuario = new Producto;
		if($usuario->setId($_GET['id'])){
			
				if(isset($_POST['eliminar'])){
					if($usuario->deleteProducto()&&$usuario->deleteInventario()){
						Page::showMessage(1, "Producto eliminado", "index.php");
					}else{
						throw new Exception(Database::getException());
					}
				}
			
		}else{
			throw new Exception("Producto incorrecto");
		}
	}else{
		Page::showMessage(3, "Seleccione un producto", "index.php");
	}
}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "index.php");
}
require_once("../../app/views/producto/delete_view.php");
?>