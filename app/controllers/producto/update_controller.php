<?php
require_once("../../app/models/producto.class.php");
require_once("../../app/models/modelo.class.php");
require_once("../../app/models/tipo_producto.class.php");
try{
    $usuario = new Producto;
    $modelo=new Modelo;
    $tipop=new TipoProducto;
    if($usuario->setId($_GET['id']))
   {
        if($usuario->readProducto())
        {
            if(isset($_POST['editar'])){
                $_POST = $usuario->validateForm($_POST);
                if($usuario->setCodigo($_POST['codigo'])){
                    if($usuario->setProducto($_POST['nombre'])){
                        if($usuario->setDescripcion($_POST['descripcion'])){
                            if($usuario->setIdModelo($_POST['modelo'])){
                                if($usuario->setIdTipoP($_POST['tipop'])){
                                    if($usuario->updateProducto()){
                                        Page::showMessage(1, "Producto actualizado", "index.php");
                                    }else{
                                        
                                        throw new Exception("No se pudo actualizar el producto, intentelo mas tarde");
                                    }
                                }else{
                                    throw new Exception("Tipo de producto incorrecto");
                                }   
                            }else{
                                throw new Exception("Modelo incorrecto");
                            }   
                        }else{
                            throw new Exception("Descripcion incorrecta");
                        }            
                    }else{
                        throw new Exception("Nombre incorrecto");
                    }              
                }else{
                    throw new Exception("Codigo incorrecto");
                }
            }
        }else{
            throw new Exception("Error");
        }
   }else{
    Page::showMessage(2,"Seleccione un producto","index.php");
   } 
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
    require_once("../../app/views/producto/update_view.php");
}
require_once("../../app/views/producto/update_view.php");
?>