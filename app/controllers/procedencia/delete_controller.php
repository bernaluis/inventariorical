<?php
require_once("../../app/models/procedencia.class.php");
try{
	if(isset($_GET['id'])){
		$usuario = new Procedencia;
		if($usuario->setId($_GET['id'])){
			if(isset($_POST['eliminar'])){
				if($usuario->borrarProcedencia()){
					Page::showMessage(1, "Lugar de procedencia eliminado", "index.php");
				}else{
					throw new Exception(Database::getException());
				}
			}
		}else{
			throw new Exception("Error");	
		}
	}else{
		Page::showMessage(3, "Seleccione un lugar", "index.php");
	}
}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "index.php");
}
require_once("../../app/views/procedencia/delete_view.php");
?>