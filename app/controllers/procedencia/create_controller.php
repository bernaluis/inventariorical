<?php
require_once("../../app/models/procedencia.class.php");
try{
    $user=new Procedencia;
    if(isset($_POST['crear'])){
        $_POST = $user->validateForm($_POST);
        if($user->setProcedencia($_POST['nombre'])){
            if($user->insertProcedencia()){
                Page::showMessage(1, "Lugar de procedencia registrado", "index.php");
              }else{
                  throw new Exception("No se pudo guardar el lugar de procedencia, intentelo mas tarde");
              }
        }else{
            throw new Exception("Nombre incorrecto");
        }
    }        
    
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
    require_once("../../app/views/procedencia/create_view.php");
}
require_once("../../app/views/procedencia/create_view.php");
?>