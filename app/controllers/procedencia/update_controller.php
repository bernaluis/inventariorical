<?php
require_once("../../app/models/procedencia.class.php");
try{
    $user=new Procedencia;
   if($user->setId($_GET['id']))
   {
        if($user->readProcedencia())
        {
            if(isset($_POST['editar'])){
                $_POST = $user->validateForm($_POST);
                if($user->setProcedencia($_POST['nombre'])){
                    if($user->updateProcedencia()){
                        Page::showMessage(1, "Lugar de procedencia actualizado", "index.php");
                      }else{
                          throw new Exception("No se pudo actualizar el lugar de procedencia, intentelo mas tarde");
                      }
                }else{
                    throw new Exception("Nombre incorrecto");
                }
            }        
        }
        else{
            Page::showMessage(2,"Nombre incorrecto","index.php");
        }
   }
   else
   {
        Page::showMessage(2,"Nombre incorrecto","index.php");
   }
    
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
   
}
require_once("../../app/views/procedencia/update_view.php");
?>