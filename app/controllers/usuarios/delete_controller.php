<?php
require_once("../../app/models/usuario.class.php");
try{
	if(isset($_GET['id'])){
		$usuario = new Usuario;
		if($usuario->setId($_GET['id'])){
			if(isset($_POST['eliminar'])){
				if($usuario->getId()==$_SESSION['id'])
				{
					throw new Exception("No te puedes borrar a ti mismo");
				}
				else{
					if($usuario->deleteUsuario()){
						Page::showMessage(1, "Usuario eliminado", "index.php");
					}else{
						throw new Exception(Database::getException());
					}
				}
			}
		}else{
			throw new Exception("Usuario incorrecto");
		}
	}else{
		Page::showMessage(3, "Seleccione un usuario", "index.php");
	}
}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "index.php");
}
require_once("../../app/views/usuarios/delete_view.php");
?>