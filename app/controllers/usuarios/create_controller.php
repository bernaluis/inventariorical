<?php
require_once("../../app/models/usuario.class.php");
try{
    $usuario = new Usuario;
    if(isset($_POST['crear'])){
        $_POST = $usuario->validateForm($_POST);
        if($usuario->setNombres($_POST['nombreEncargado'])){
            if($usuario->setApellidos($_POST['apellidosEncargado'])){
                if($usuario->setCorreo($_POST['correoEncargado']))
                {
                    if($usuario->setCodigo($_POST['usuarioEncargado']))
                    {
                        if($_POST['contraEncargado']==$_POST['confirmaContra'])
                        {
                            if($usuario->setContra($_POST['contraEncargado']))
                            {
                                if($usuario->setIdTipoU($_POST['tipo']))
                                {
                                    if($usuario->insertUsuario()){
                                        Page::showMessage(1, "Usuario registrado", "index.php");
                                    }else{
                                        throw new Exception("No se pudo guardar el usuario, intentelo mas tarde");
                                    }
                                }
                                else
                                {
                                    throw new Exception("Tipo de usuario incorrecto");
                                }
                            }
                            else{
                                Page::showMessage(2, "Contraseña incorrecta", "create.php");
                            }
                        }else{
                            throw new Exception("Las contraseñas no coinciden");
                        }   
                    }
                    else{
                        throw new Exception("Usuario incorrecto");
                    }   
                }
                else{
                    throw new Exception("Correo incorrecto");
                }
            }else{
                throw new Exception("Apellidos incorrectos");
            }            
        }else{
            throw new Exception("Nombres incorrectos");
        }        
    }
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
}
require_once("../../app/views/usuarios/create_view.php");
?>