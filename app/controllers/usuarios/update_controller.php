<?php
require_once("../../app/models/usuario.class.php");
try{
    $usuario = new Usuario;
    if($usuario->setId($_GET['id'])){
        if($usuario->readUsuario()){
            if(isset($_POST['editar'])){
                $_POST = $usuario->validateForm($_POST);
                if($usuario->setNombres($_POST['nombreEncargado'])){
                    if($usuario->setApellidos($_POST['apellidosEncargado'])){
                        if($usuario->setCorreo($_POST['correoEncargado']))
                        {
                            if($usuario->setCodigo($_POST['usuarioEncargado']))
                            {
                                if($_POST['contraEncargado']==null){
                                    if($usuario->setIdTipoU($_POST['tipo']))
                                    {
                                        if($usuario->updateUsuario2()){
                                            Page::showMessage(1, "Usuario actualizado", "index.php");
                                        }else{
                                            throw new Exception("No se pudo actualizar el usuario, intentelo mas tarde");
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("Tipo de usuario incorrecto");
                                    }
                                }else{
                                    if($_POST['contraEncargado']==$_POST['confirmaContra'])
                                    {
                                        if($usuario->setContra($_POST['contraEncargado']))
                                        {
                                            if($usuario->setIdTipoU($_POST['tipo']))
                                            {
                                                if($usuario->updateUsuario()){
                                                    Page::showMessage(1, "Usuario actualizado", "index.php");
                                                }else{
                                                    throw new Exception("No se pudo actualizar el usuario, intentelo mas tarde");
                                                }
                                            }
                                            else
                                            {
                                                throw new Exception("Tipo de usuario incorrecto");
                                            }
                                        }
                                        else{
                                            throw new Exception("Contraseña incorrecta");
                                        }
                                    }else{
                                        throw new Exception("Las contraseñas no coinciden");
                                    }   
                                }
                            }
                            else{
                                throw new Exception("Usuario incorrecto");
                            }   
                        }
                        else{
                            throw new Exception("Correo incorrecto");
                        }
                    }else{
                        throw new Exception("Apellidos incorrectos");
                    }            
                }else{
                    throw new Exception("Nombres incorrectos");
                }        
            }
        }else{
            Page::showMessage(2, "Usuario inexistente", "index.php");
        }
    }else{
        Page::showMessage(2, "Usuario incorrecto", "index.php");
    }
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
}
require_once("../../app/views/usuarios/update_view.php");

?>