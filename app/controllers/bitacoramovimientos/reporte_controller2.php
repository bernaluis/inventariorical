<?php

include($_SERVER['DOCUMENT_ROOT'] . '/inventariorical/app/controllers/plantilla.php');;
require_once($_SERVER['DOCUMENT_ROOT'] . '/inventariorical/app/Libraries/fpdf/fpdf.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/inventariorical/app/models/movimientos.class.php');


$title = 'Reporte de movimientos';
$user=new Movimientos();
$pdf   = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetMargins(5 , 25, 5); // Margenes izq y derc, arriba 
try{
    if(isset($_GET['id'])){
        $pdf->SetFont('Helvetica','',11);
                    $pdf->Cell(190,10,'Reporte de solicitud de movimiento.',0,1,'C');
                    
                   
                    $data1 = $user->getReport3($_GET['id']);
                    
                    if($data1){
                        //tabla 1
                        $pdf->Ln();
                        $date=date('Y-m-d');
                        $pdf->Cell(0,10,utf8_decode('Fecha '.$date),0,1,'C');
                        
                        $pdf->SetFont('Helvetica','B',8); // Aplicamos tipo Bold
                        $header1 = array('Producto','Administrador','Usuario','Bodega','Estado','Tipo','Proceso','Cantidad','Procedencia');
                        $pdf->Tabla($header1,$data1);

                        
                    }else{
                        throw new Exception('No hay datos disponibles');
                    }
        
    }else{
        throw new Exception('Debe seleccionar un movimiento');
    }

}catch(Exception $error){
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(17, 10, $error->getMessage(), 0, 'L');
}
$pdf->Output();