<?php
require_once("../../app/models/movimientos.class.php");
// Requiriendo archivos

require_once($_SERVER['DOCUMENT_ROOT'] . '/inventariorical/app/models/tipo_movimiento.class.php');

$movimiento=new TipoMovimiento();

try{
	$usuario = new Movimientos;

		if(isset($_POST['buscar'])){
			$_POST = $usuario->validateForm($_POST);
			$data = $usuario->searchMovimientosBita($_POST['busqueda']);
			if($data){
				$rows = count($data);
				Page::showMessage(4, "Se encontraron $rows resuldatos", null);
				$totalPaginas=0;
			}else{
				Page::showMessage(4, "No se encontraron resultados", null);
				$data = $usuario->consultarMovimientosBita();
				$totalPaginas=0;
			}
		}else{
			//obtener valores del metodo
			$dataG = $usuario->consultarMovimientosBita();
			//contar cuantos datos hay
			$row= count($dataG);
			//cantidad de horarios por pagina
			$tamanioPag=8;
			//si hay en la url el valor pagina
			if(isset($_GET['pagina']))
			{
				//si el valor de pagina es =1
				if($_GET['pagina']==1)
				{
					header("location:index.php");
				}
				else
				{
					$pagina=$_GET['pagina'];
				}
			}
			else{
				$pagina=1;
			}
			//valor desde el que se va a comenzar a mostrar
			$empezarDesde=($pagina-1)*$tamanioPag;
			//obtener la cantidad de paginas a mostrar
			$totalPaginas=ceil($row/$tamanioPag);
			//metoodo de obtener platos con limites
			$data = $usuario->consultarMovimientosLimiteBita($empezarDesde,$tamanioPag);
		}
		if($data){
			require_once("../../app/views/bitacoramovimientos/index_view.php");

		}else{
			require_once("../../app/views/bitacoramovimientos/index_view.php");

		}

}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "../account/");
}
?>