<?php

include($_SERVER['DOCUMENT_ROOT'] . '/inventariorical/app/controllers/plantilla.php');;
require_once($_SERVER['DOCUMENT_ROOT'] . '/inventariorical/app/Libraries/fpdf/fpdf.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/inventariorical/app/models/movimientos.class.php');


$title = 'Reporte de movimientos';
$user=new Movimientos();
$pdf   = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetMargins(5 , 25, 5); // Margenes izq y derc, arriba 
try{
    if(isset($_POST['repo'])){
        if(isset($_POST['fecha1'])){
            if(isset($_POST['fecha2'])){
                $time1=strtotime($_POST['fecha1']);
                $time2=strtotime($_POST['fecha2']);
                if($time1<$time2){
                    $pdf->SetFont('Helvetica','',11);
                    $pdf->Cell(190,10,'INFORME DESDE EL '.$_POST['fecha1'].' AL '.$_POST['fecha2'].'.',0,1,'C');
                    
                    $pdf->Cell(0,10,utf8_decode('Reporte de movimientos filtrado por tipo'),0,1,'C');
                    $data1 = $user->getReport1($_POST['movimiento'],$_POST['fecha1'],$_POST['fecha2']);
                    
                    if($data1){
                        //tabla 1
                        $pdf->Ln();
                        
                        
                        $pdf->SetFont('Helvetica','B',8); // Aplicamos tipo Bold
                        $header1 = array('Producto','Administrador','Usuario','Bodega','Estado','Tipo','Proceso','Cantidad','Fecha');
                        $pdf->Tabla($header1,$data1);

                        
                    }else{
                        throw new Exception('No hay datos disponibles');
                    }
                }else{
                    throw new Exception('La fecha de inicio no debe ser mayor a la de fin');
                }
            }else{
                throw new Exception('Debe seleccionar una fecha de fin');
            }
        }else{
            throw new Exception('Debe seleccionar una fecha de inicio');
        }
    }else if(isset($_POST['repog'])){
        $pdf->SetFont('Helvetica','',11);
        
        $pdf->Cell(0,10,utf8_decode('Reporte de movimientos general'),0,1,'C');
        $data1 = $user->getReport2();
        
        if($data1){
            //tabla 1
            $pdf->Ln();
            
            
            $pdf->SetFont('Helvetica','B',8); // Aplicamos tipo Bold
            $header1 = array('Producto','Administrador','Usuario','Bodega','Estado','Tipo','Proceso','Cantidad','Fecha');
            $pdf->Tabla($header1,$data1);

            
        }else{
            throw new Exception('No hay datos disponibles');
        }
    }else{
        throw new Exception('Debe seleccionar un tipo de movimiento');
    }

}catch(Exception $error){
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(17, 10, $error->getMessage(), 0, 'L');
}
$pdf->Output();