<?php
require_once("../../app/models/estado_producto.class.php");
try{
	if(isset($_GET['id'])){
		$usuario = new EstadoP;
		if($usuario->setId($_GET['id'])){
			
				if(isset($_POST['eliminar'])){
					if($usuario->borrarEstado()){
						Page::showMessage(1, "Estado eliminado", "index.php");
					}else{
						throw new Exception(Database::getException());
					}
				}
		
			
		}else{
			throw new Exception("Error");	
		}
	}else{
		Page::showMessage(3, "Seleccione un estado", "index.php");
	}
}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "index.php");
}
require_once("../../app/views/estado_producto/delete_view.php");
?>