<?php
require_once("../../app/models/estado_producto.class.php");
try{
	$usuario = new EstadoP;

		if(isset($_POST['buscar'])){
			$_POST = $usuario->validateForm($_POST);
			$data = $usuario->searchEstado($_POST['busqueda']);
			if($data){
				$rows = count($data);
				Page::showMessage(4, "Se encontraron $rows resuldatos", null);
				$totalPaginas=0;
			}else{
				Page::showMessage(4, "No se encontraron resultados", null);
				$data = $usuario->consultarEstado();
				$totalPaginas=0;
			}
		}else{
			//obtener valores del metodo
			$dataG = $usuario->consultarEstado();
			//contar cuantos datos hay
			$row= count($dataG);
			//cantidad de horarios por pagina
			$tamanioPag=5;
			//si hay en la url el valor pagina
			if(isset($_GET['pagina']))
			{
				//si el valor de pagina es =1
				if($_GET['pagina']==1)
				{
					header("location:index.php");
				}
				else
				{
					$pagina=$_GET['pagina'];
				}
			}
			else{
				$pagina=1;
			}
			//valor desde el que se va a comenzar a mostrar
			$empezarDesde=($pagina-1)*$tamanioPag;
			//obtener la cantidad de paginas a mostrar
			$totalPaginas=ceil($row/$tamanioPag);
			//metoodo de obtener platos con limites
			$data = $usuario->consultarEstadoLimite($empezarDesde,$tamanioPag);
		}
		if($data){
			require_once("../../app/views/estado_producto/index_view.php");

		}else{
			require_once("../../app/views/estado_producto/index_view.php");

		}

}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "../account/");
}
?>