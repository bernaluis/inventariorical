<?php
require_once("../../app/models/estado_producto.class.php");
try{
    $user=new EstadoP;
   if($user->setId($_GET['id']))
   {
        if($user->readEstado())
        {
            if(isset($_POST['editar'])){
                $_POST = $user->validateForm($_POST);
                if($user->setEstado($_POST['nombre'])){
                    if($user->updateEstado()){
                        Page::showMessage(1, "Estado actualizado", "index.php");
                      }else{
                          throw new Exception("No se pudo actualizar el estado, intentelo mas tarde");
                      }
                }else{
                    throw new Exception("Nombre incorrecto");
                }
            }        
        }
        else{
            Page::showMessage(2,"Nombre incorrecto","index.php");
        }
   }
   else
   {
        Page::showMessage(2,"Nombre incorrecto","index.php");
   }
    
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
   
}
require_once("../../app/views/estado_producto/update_view.php");
?>