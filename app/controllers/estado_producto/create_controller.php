<?php
require_once("../../app/models/estado_producto.class.php");
try{
    $user=new EstadoP;
    if(isset($_POST['crear'])){
        $_POST = $user->validateForm($_POST);
        if($user->setEstado($_POST['nombre'])){
            if($user->insertEstado()){
                Page::showMessage(1, "Estado registrado", "index.php");
              }else{
                  throw new Exception("No se pudo guardar el estado, intentelo mas tarde");
              }
        }else{
            throw new Exception("Nombre incorrecto");
        }
    }        
    
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
    require_once("../../app/views/estado_producto/create_view.php");
}
require_once("../../app/views/estado_producto/create_view.php");
?>