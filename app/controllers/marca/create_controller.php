<?php
require_once("../../app/models/marca.class.php");
try{
    $user=new Marca;
    if(isset($_POST['crear'])){
        $_POST = $user->validateForm($_POST);
        if($user->setMarca($_POST['nombre'])){
            if($user->insertMarca()){
                Page::showMessage(1, "Marca registrada", "index.php");
              }else{
                  throw new Exception("No se pudo guardar la marca, intentelo mas tarde");
              }
        }else{
            throw new Exception("Nombre de marca incorrecto");
        }
    }        
    
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
    require_once("../../app/views/marca/create_view.php");
}
require_once("../../app/views/marca/create_view.php");
?>