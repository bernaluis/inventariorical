<?php
require_once("../../app/models/marca.class.php");
try{
    $user=new Marca;
   if($user->setId($_GET['id']))
   {
        if($user->readMarca())
        {
            if(isset($_POST['editar'])){
                $_POST = $user->validateForm($_POST);
                if($user->setMarca($_POST['nombre'])){
                    if($user->updateMarca()){
                        Page::showMessage(1, "Marca actualizada", "index.php");
                      }else{
                          throw new Exception("No se pudo actualizar la marca, intentelo mas tarde");
                      }
                }else{
                    throw new Exception("Nombre de marca incorrecto");
                }
            }        
        }
        else{
            Page::showMessage(2,"Marca incorrecta","index.php");
        }
   }
   else
   {
        Page::showMessage(2,"Marca incorrecta","index.php");
   }
    
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
   
}
require_once("../../app/views/marca/update_view.php");
?>