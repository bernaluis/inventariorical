<?php
require_once("../../app/models/marca.class.php");
try{
	if(isset($_GET['id'])){
		$usuario = new Marca;
		if($usuario->setId($_GET['id'])){
			if(isset($_POST['eliminar'])){
				if($usuario->borrarMarca()){
					Page::showMessage(1, "Marca eliminada", "index.php");
				}else{
					throw new Exception(Database::getException());
				}
			}
		}else{
			throw new Exception("Marca incorrecta");	
		}
	}else{
		Page::showMessage(3, "Seleccione una marca", "index.php");
	}
}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "index.php");
}
require_once("../../app/views/marca/delete_view.php");
?>