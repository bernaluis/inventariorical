<?php
require_once("../../app/models/usuario.class.php");
try{
	$object = new Usuario;
	if(isset($_SESSION['id'])&&isset($_SESSION['usuario'])&&isset($_SESSION['tipo'])){
		Page::showMessage(1, "Ya hay una sesion iniciada" , "index.php");
	}
	else{	
		if($object->getUsuariosVal()){

			if(isset($_POST['sesion'])){
				
					$_POST = $object->validateForm($_POST);
					if($object->setCodigo($_POST['alias'])){
						if($object->checkUser()){
							
								//se setea la clave
								if ($object->setContra($_POST['contra'])) {
								//verifica que la contraseña ingresada sea igual a la del usuario
									if($object->checkPassword()){
										//variables de sesion
										$_SESSION['id'] = $object->getId();
										$_SESSION['usuario'] = $object->getCodigo();
										$_SESSION['tipo']   = $object->getIdTipoU();
										Page::showMessage(1, "Autenticación correcta", "index.php");	
									} else {
										
										throw new Exception("Credenciales incorrectas :(");

									}	
								} else {
									throw new Exception("Clave erronea");
								}
						}else{
						
							throw new Exception("Alias inexistente");
						}
					}else{
						
						throw new Exception("Alias incorrecto");
					}
				}
				if(isset($_POST['recuperar'])){
				
					$_POST = $object->validateForm($_POST);
					if($object->setCodigo($_POST['alias2'])){
						if($object->checkUser()){
							$passw = rand(999, 99999);
							$object->setContra($passw);
							$object->changePasswordRecuperar();
							$to=$object->getCorreo();
							$subject="Cambio de contraseña";
							$message="Su nueva contraseña es ".strval($passw)." ";
							$headers = 'From: inventariorical@gmail.com' . "\r\n" .
							'Reply-To: inventariorical@gmail.com' . "\r\n" .
							'X-Mailer: PHP/' . phpversion();
							if(mail($to, $subject, $message, $headers)){
								Page::showMessage(1, "Contraseña reestablecida", "");	
							}else{
								Page::showMessage(3, "Error, contacte al administrador", "");	
							}
							
									
						}else{
						
							throw new Exception("Alias inexistente");
						}
					}else{
						
						throw new Exception("Alias incorrecto");
					}
				}
			}
			
			
		
		else{
			Page::showMessage(3, "No hay registros, contacte al administrador","");
		}
	}
}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), null);
}
require_once("../../app/views/account/login_view.php");
require_once("../../app/views/account/recuperarContra_view.php");
?>