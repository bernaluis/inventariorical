<?php
require_once("../../app/models/usuario.class.php");
try{
    if(isset($_POST['modiContra'])){
        $usuario = new Usuario;
        $_POST = $usuario->validateForm($_POST);
        if($usuario->setId($_SESSION['id'])){
            if($_POST['contraanterior'] != null){
                if($usuario->setContra($_POST['contraanterior'])){
                    if($usuario->checkPasswordP()){
                        if($_POST['contrauno'] == $_POST['contrados']){
                            if($usuario->setContra($_POST['contrauno'])){
                                if ($usuario->changePassword()) {
                                    //mensaje de contra cambiada
                                    Page::showMessage(1, "Clave cambiada", "../account/index.php"); 
                                    
                                  
                                } else {
                                    throw new Exception(Database::getException());
                                }
                            }else{
                                throw new Exception("Clave nueva menor a 6 caracteres");
                            }
                        }else{
                            throw new Exception("Claves nuevas diferentes");
                        }
                    }else{
                        throw new Exception("Clave actual ingresada no coincide con la clave guardada");
                    }
                }else{
                    throw new Exception("Clave actual menor a 6 caracteres");
                }
            }else{
                throw new Exception("Debe ingresar la contraseña actual");
            }
        }else{
            Page::showMessage(2, "Usuario incorrecto", "index.php");
        }
    }
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
}
require_once("../../app/views/account/password_view.php");
?>