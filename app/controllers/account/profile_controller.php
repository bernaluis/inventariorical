<?php
require_once("../../app/models/usuario.class.php");
try{
    $usuario = new Usuario;
    if($usuario->setId($_SESSION['id'])){
        if($usuario->readUsuario()){
            if(isset($_POST['editar'])){
                $_POST = $usuario->validateForm($_POST);
                if($usuario->setNombres($_POST['nombre'])){
                    if($usuario->setApellidos($_POST['apellido'])){
                        if($usuario->setCorreo($_POST['correo'])){
                            if($usuario->setCodigo($_POST['alias'])){
                                if($usuario->updatePerfil()){
                                    $_SESSION['usuario'] = $usuario->getCodigo();
                                    Page::showMessage(1, "Perfil modificado", "index.php");
                                }else{
                                    throw new Exception(Database::getException());
                                }
                            }else{
                                throw new Exception("Alias incorrecto");
                            }
                        }else{
                            throw new Exception("Correo incorrecto");
                        }
                    }else{
                        throw new Exception("Apellidos incorrectos");
                    }
                }else{
                    throw new Exception("Nombres incorrectos");
                }
            }
        }else{
            Page::showMessage(2, "Usuario inexistente", "index.php");
        }
    }else{
        Page::showMessage(2, "Usuario incorrecto", "index.php");
    }
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
}
require_once("../../app/views/account/profile_view.php");
require_once("password_controller.php");
?>