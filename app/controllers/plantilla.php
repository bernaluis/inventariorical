<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/inventariorical/app/libraries/fpdf/fpdf.php');

class PDF extends FPDF
{
    // Cabecera de página
    function Header()
    {
        global $title;
        $this->Image('../../web/img/logo.png',10,10,20,20,'PNG');
        $this->SetFont('Helvetica','',20); // Agregamos fuente
        // Mover a la derecha
        $this->Cell(20,10);
        // Título
        $this->Cell(20,10);
        $this->Cell(110,10,utf8_decode('INSTITUTO TECNICO RICALDONE'),0,1,'R');
        $this->Cell(40,10);

        $this->SetFont('Helvetica','',14); // Agregamos fuente
        $this->Cell(20,10);
        $this->Cell(95,5,utf8_decode('Avenida Aguilares 218'),0,1,'L');
        $this->Cell(50,10);
        $this->Cell(95,5,utf8_decode('San Salvador, El Salvador, C.A.'),0,1,'L');
        $this->Ln(15);
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // $this->Line(200,280,10,280);
        // Arial italic 8
        // $this->SetFont('Helvetica','B',12);
        // $this->Cell(0,7,utf8_decode('Con una visión más humana al servicio integral de su salud'),0,1,'C');
        $this->SetFont('Helvetica','I',8);
        // Número de página
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
    // Colored table
    function Tabla1($header, $data)
    {
        // Colors, line width and bold font
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0);
        $this->SetDrawColor(128,0,0);
        $this->SetLineWidth(.3);
        $this->SetFont('','B');
        // Header
        $w = array(30, 22);
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[1],7,iconv('UTF-8', 'windows-1252', $header[$i]),1,0,'C',true);
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(255,255,255);
        $this->SetTextColor(0);
        $this->SetFont('Arial');
        // Data
        $fill = false;
        foreach($data as $row)
        {
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[0]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[1]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[2]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[3]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[4]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[5]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[6]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[7]),'LR',0,'L',$fill);
            $this->Cell($w[0],6,iconv('UTF-8', 'windows-1252', $row[8]),'LR',0,'L',$fill);
            //$this->Cell($w[4],6,iconv('UTF-8', 'windows-1252', $row[4].' '.$row[5]),'LR',0,'R',$fill);
            $this->Ln();
            $fill = !$fill;
        }
        // Closing line
        $this->Cell(array_sum($w),0,'','T');
    }
    function Tabla($header, $data)
    {
        // Colors, line width and bold font
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0);
        $this->SetDrawColor(128,0,0);
        $this->SetLineWidth(.3);
        $this->SetFont('','B');
        // Header
        $w = array(80, 22);
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[1],7,iconv('UTF-8', 'windows-1252', $header[$i]),1,0,'C',true);
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(255,255,255);
        $this->SetTextColor(0);
        $this->SetFont('Arial');
        // Data
        $fill = false;
        foreach($data as $row)
        {
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[0]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[1]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[2]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[3]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[4]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[5]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[6]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[7]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[8]),'LR',0,'L',$fill);
            //$this->Cell($w[4],6,iconv('UTF-8', 'windows-1252', $row[4].' '.$row[5]),'LR',0,'R',$fill);
            $this->Ln();
            $fill = !$fill;
        }
        // Closing line
        $this->Cell(array_sum($w),0,'','T');
    }
    // Colored table
    function TablaInve($header, $data)
    {
        // Colors, line width and bold font
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0);
        $this->SetDrawColor(128,0,0);
        $this->SetLineWidth(.3);
        $this->SetFont('','B');
        // Header
        $w = array(80,29);
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[1],7,iconv('UTF-8', 'windows-1252', $header[$i]),1,0,'C',true);
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(255,255,255);
        $this->SetTextColor(0);
        $this->SetFont('Arial');
        // Data
        $fill = false;
        
        
        foreach($data as $row)
        {
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[0]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[1]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[2]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[3]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[4]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[5]),'LR',0,'L',$fill);
            $this->Cell($w[1],6,iconv('UTF-8', 'windows-1252', $row[6]),'LR',0,'L',$fill);
            

            $fill = !$fill;
            
            $this->Ln();
        }
       
        //$this->Cell($w[4],6,iconv('UTF-8', 'windows-1252', $row[4].' '.$row[5]),'LR',0,'R',$fill);
        
        $fill = !$fill;
        // Closing line
        $this->Cell(array_sum($w),0,'','T');
    }
}
?>