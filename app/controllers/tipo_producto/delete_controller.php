<?php
require_once("../../app/models/tipo_producto.class.php");
try{
	if(isset($_GET['id'])){
		$usuario = new TipoProducto;
		if($usuario->setId($_GET['id'])){
			$data=$usuario->verificarDelete();
			if(!$data){
				if(isset($_POST['eliminar'])){
					if($usuario->borrarTipoP()){
						Page::showMessage(1, "Tipo de producto eliminado", "index.php");
					}else{
						throw new Exception(Database::getException());
					}
				}
		
			}else{
				throw new Exception("Hay productos asignados a este tipo, modifique antes de eliminar");
			}
		}else{
			throw new Exception("Tipo incorrecto");	
		}
	}else{
		Page::showMessage(3, "Seleccione un tipo", "index.php");
	}
}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "index.php");
}
require_once("../../app/views/tipo_producto/delete_view.php");
?>