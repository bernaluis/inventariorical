<?php
require_once("../../app/models/tipo_producto.class.php");
try{
    $user=new TipoProducto;
    if(isset($_POST['crear'])){
        $_POST = $user->validateForm($_POST);
        if($user->setTipoP($_POST['nombre'])){
            if($user->insertTipoP()){
                Page::showMessage(1, "Tipo de producto registrado", "index.php");
              }else{
                  throw new Exception("No se pudo guardar el tipo de producto, intentelo mas tarde");
              }
        }else{
            throw new Exception("Nombre incorrecto");
        }
    }        
    
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
    require_once("../../app/views/tipo_producto/create_view.php");
}
require_once("../../app/views/tipo_producto/create_view.php");
?>