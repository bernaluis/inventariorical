<?php
require_once("../../app/models/tipo_producto.class.php");
try{
    $user=new TipoProducto;
   if($user->setId($_GET['id']))
   {
        if($user->readTipoP())
        {
            if(isset($_POST['editar'])){
                $_POST = $user->validateForm($_POST);
                if($user->setTipoP($_POST['nombre'])){
                    if($user->updateTipoP()){
                        Page::showMessage(1, "Tipo de producto actualizado", "index.php");
                      }else{
                          throw new Exception("No se pudo actualizar el tipo de producto, intentelo mas tarde");
                      }
                }else{
                    throw new Exception("Nombre incorrecto");
                }
            }        
        }
        else{
            Page::showMessage(2,"Tipo incorrecto","index.php");
        }
   }
   else
   {
        Page::showMessage(2,"Tipo incorrecto","index.php");
   }
    
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
   
}
require_once("../../app/views/tipo_producto/update_view.php");
?>