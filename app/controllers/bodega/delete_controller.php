<?php
require_once("../../app/models/bodega.class.php");
try{
	if(isset($_GET['id'])){
		$usuario = new Bodega;
		if($usuario->setId($_GET['id'])){
			$data=$usuario->verificarDelete();
			if(!$data){
				if(isset($_POST['eliminar'])){
					if($usuario->borrarBodega()){
						Page::showMessage(1, "Bodega eliminada", "index.php");
					}else{
						throw new Exception(Database::getException());
					}
				}
		
			}else{
				throw new Exception("Hay inventario en esta bodega, modifique antes de eliminar");
			}
		}else{
			throw new Exception("Error");	
		}
	}else{
		Page::showMessage(3, "Seleccione una bodega", "index.php");
	}
}catch(Exception $error){
	Page::showMessage(2, $error->getMessage(), "index.php");
}
require_once("../../app/views/bodega/delete_view.php");
?>