<?php
require_once("../../app/models/bodega.class.php");
try{
    $user=new Bodega;
   if($user->setId($_GET['id']))
   {
        if($user->readBodega())
        {
            if(isset($_POST['editar'])){
                $_POST = $user->validateForm($_POST);
                if($user->setBodega($_POST['nombre'])){
                    if($user->updateBodega()){
                        Page::showMessage(1, "Bodega actualizada", "index.php");
                      }else{
                          throw new Exception("No se pudo actualizar la bodega, intentelo mas tarde");
                      }
                }else{
                    throw new Exception("Nombre incorrecto");
                }
            }        
        }
        else{
            Page::showMessage(2,"Nombre incorrecto","index.php");
        }
   }
   else
   {
        Page::showMessage(2,"Nombre incorrecto","index.php");
   }
    
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
   
}
require_once("../../app/views/bodega/update_view.php");
?>