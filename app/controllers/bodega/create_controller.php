<?php
require_once("../../app/models/bodega.class.php");
try{
    $user=new Bodega;
    if(isset($_POST['crear'])){
        $_POST = $user->validateForm($_POST);
        if($user->setBodega($_POST['nombre'])){
            if($user->insertBodega()){
                Page::showMessage(1, "Bodega registrada", "index.php");
              }else{
                  throw new Exception("No se pudo guardar la bodega, intentelo mas tarde");
              }
        }else{
            throw new Exception("Nombre incorrecto");
        }
    }        
    
}catch(Exception $error){
    Page::showMessage(2, $error->getMessage(), null);
    require_once("../../app/views/bodega/create_view.php");
}
require_once("../../app/views/bodega/create_view.php");
?>